| High-end CMYK support | WIP |  As a backend pixel storage. See [early experiments](https://twitter.com/CmykStudent/status/1618261982628704259). Also see [the main GIMP FAQ](https://www.gimp.org/docs/userfaq.html#i-do-a-lot-of-desktop-publishing-related-work-will-you-ever-support-cmyk) for details |
| Spot Color support | No | Ability to add more channels for printing. |
| Reviewing/Improving Indexed color mode | No | Our Indexed mode is limited and not so well maintained. Make a review of how people use it and how to improve it would be a nice project. |
