| Page support | WIP |  |
| Core animation | WIP |  Animation plug-in is dropped, this is being rewritten as a core feature |
| Story-board mode | WIP |  |
| New XCF format | Discussed | An archive-based format should allow us more easily to load data on-use, therefore allowing much bigger project files (ex.: page and animation)|
| Improved export | WIP | More ability to export full-document, per-page, per layers… Web exports and alternative views were also discussed. |
| Auto-save | No | Implementation details are still being discussed; it might pass through a new XCF version based on GEGL buffers. |
