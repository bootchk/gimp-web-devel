| Paint Select tool | playground | Released as experimental in 2.99.4, needs work |
| Seamless Clone tool | playground | Released as experimental in 2.9.2, needs work |
| N-point deformation tool | playground | Released as experimental in 2.9.2, needs work |
| Particle selection tool |  No | See [comment on issue 2912](https://gitlab.gnome.org/GNOME/gimp/-/issues/2912#note_431542) The Foreground selection interaction should be reworked as a tool to select particles or object borders |
| Inpainting tool | No | Feature long-available with resynthesizer third-party plug-in; would deserve GEGL-based core implementation |
| Text tool re-work | WIP | This tool deserves a big rewrite of interaction (closer to well-known interactions, such as in LibreOffice); see also a [proposal by Liam](https://gui.gimp.org/index.php?title=OnCanvasTextEditing) - [part of gsoc 23 (#9274)](https://gitlab.gnome.org/GNOME/gimp/-/issues/9274) |
| Text tool: OpenType support | No | See [proposal in-progress](https://gui.gimp.org/index.php?title=OpenTypeSupport) |
| Better "smooth painting" in Paint tools | No | Find a proper algorithm to have good smooth painting at last! |
