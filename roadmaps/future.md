| Optionally save undo history in the XCF |  No |  [#89](https://gitlab.gnome.org/GNOME/gimp/issues/89) |
| Brushpack support |  No | |
| Improve resource creation |  No | Dedicated GUI to create brushes, patterns, or other resources would be welcome |
