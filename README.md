developer.gimp.org
------------------

This git repository holds the source for [GIMP's developer
website](https://developer.gimp.org/).

## Bootstrap and build

This website uses git submodules to include the base theme to build from (the
theme is [hugo-bootstrap-bare](https://gitlab.com/pixlsus/hugo-bootstrap-bare)
from the pixls.us devs).

1. You'll need to pull the submodule and initialize some things:

```sh
$ git submodule update --init --recursive
```

2. Then fetch the assets:

```sh
$ cd themes/hugo-bootstrap-bare/assets
$ npm install # (or yarn install)
```

3. Install hugo using the advice on [their
   website](https://gohugo.io/getting-started/installing/).

The above steps will only need to be performed the first time or when you need
to update the dependencies.

4. Finally build with

```sh
$ hugo
```

## Testing your Changes

Rather than running `hugo` (last step in the previous section), we recommend to
test your site with:

```sh
$ hugo serve
```

The site will be hosted on your localhost with a port number decided
at launch, (by default 1313), which means your local website will likely be
viewable at the address:
[https://localhost:1313](https://localhost:1313)

This test website will be dynamically updated as you change files locally.

## Learning Hugo

To learn how to edit the site start by reading any of the markdown
files while running your local server. It will automatically update
if you make changes and it should be easy to intuit how it works.

For more advanced editing, read the
[official docs](https://gohugo.io/getting-started/usage/).

## Contributing

Commits to this module cause the website to be updated daily.
So please:

1. By contributing, you accept to license your work under [Creative Commons
   Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).
   You may request to license otherwise, in which case the license must be
   explicitly stated in the document. See the section "[License](#License)"
   below for our rules on licensing.
2. Test all your changes locally before contributing a patch.
3. Submit a [merge
   request](https://gitlab.gnome.org/Infrastructure/gimp-web-devel/-/merge_requests).
   The usual target branch is `testing` (default). Never work on the `master`
   branch, which is reserved solely for synchronizing the main website with the
   testing website.
4. Once your patch is merged, it will be visible within 24 hours on the testing
   website:
   [https://testing.developer.gimp.org/](https://testing.developer.gimp.org/)
5. The [public website](https://developer.gimp.org/) is synchronized manually by
   maintainers, from time to time, after verifying everything looks fine.

## License

By contributing new documentation, you accept to license your work under
[Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA
4.0)](https://creativecommons.org/licenses/by-sa/4.0/).

You may request to license otherwise, in which case the license must be
explicitly stated in the documents you contribute. Nevertheless we will accept
only *Libre licenses* such as: Creative Commons by-sa, Creative Commons by,
Creative Commons zero, Free Art, GNU FDL (ideally without invariant sections; we
could study on case by case basis but normally you should not need invariant
sections)…

Basically the code needs to be modifiable by other contributors, even years
later, to follow with the software's evolution and avoid displaying outdated
information. We also want people to be allowed to share our docs if needed.

Contents older than this rule (2023-05-10) may be in different licenses. Some
parts of this website come from the project's old wikis, or old developer
website. The `git log` (or the contents itself if it has specific licensing
mentions) is the authoritative source about license for older data.

*GIMP team*
