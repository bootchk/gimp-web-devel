# Script for converting wiki syntax into markdown.

# Converting the internal link format which has a label.
s/\[\[\([^|]*\)|\([^]]*\)\]\]/[\2](\1)/g

# Converting the external link format which has a label.
s/\[\(http[^ ]*\) \([^]]*\)\]/[\2](http\1)/g

# Converting the external link format which has a label without a protocol.
s/\[\(www[^ ]*\) \([^]]*\)\]/[\2](www\1)/g

# Converting headers.
s/^===== *\(.*\) *=====$/##### \1/g
s/^==== *\(.*\) *====$/#### \1/g
s/^=== *\(.*\) *===$/### \1/g
s/^== *\(.*\) *==$/## \1/g

# Converting emphasis markers.
s/'''/**/g
s/''/*/g
