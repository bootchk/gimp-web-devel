#!/bin/bash

function get_api_docs () {
  DOWNLOAD_URL="https://download.gimp.org/pub/gimp/v$1"

  echo "Looking up current revision..."
  VERSION=`wget --no-verbose -O - $DOWNLOAD_URL | grep "0.0_LATEST-IS" | sed "s/.*LATEST-IS-//;s|</a>.*||"`

  echo "Getting tarball..."
  wget --no-verbose $DOWNLOAD_URL/gimp-$VERSION.tar.bz2

  echo "Extracting..."
  tar xf gimp-$VERSION.tar.bz2

  echo "Installing tarball data into public/..."
  rm -fr public/api/2.0/*/
  mkdir -p public/api/2.0/

  mv gimp-$VERSION/devel-docs/libgimp/html public/api/2.0/libgimp
  mv gimp-$VERSION/devel-docs/libgimpbase/html public/api/2.0/libgimpbase
  mv gimp-$VERSION/devel-docs/libgimpcolor/html public/api/2.0/libgimpcolor
  mv gimp-$VERSION/devel-docs/libgimpconfig/html public/api/2.0/libgimpconfig
  mv gimp-$VERSION/devel-docs/libgimpmath/html public/api/2.0/libgimpmath
  mv gimp-$VERSION/devel-docs/libgimpmodule/html public/api/2.0/libgimpmodule
  mv gimp-$VERSION/devel-docs/libgimpthumb/html public/api/2.0/libgimpthumb
  mv gimp-$VERSION/devel-docs/libgimpwidgets/html public/api/2.0/libgimpwidgets

  mkdir -p public/core/setup/build/2.10/
  mv gimp-$VERSION/INSTALL public/core/setup/build/2.10/

  echo "Tidying up..."
  rm -fr gimp-$VERSION gimp-$VERSION.tar.bz2

  echo "Done."
}

function get_dev_api_docs () {
  APIVER=$1
  DOWNLOAD_URL="https://download.gimp.org/gimp/v$APIVER"

  echo "Looking up current revision:"
  VERSION=`wget --no-verbose -O - $DOWNLOAD_URL | grep "0.0_LATEST-IS" | sed "s/.*LATEST-IS-//;s|</a>.*||"`

  echo "Getting tarball..."
  wget --no-verbose $DOWNLOAD_URL/api-docs/gimp-api-docs-$VERSION.tar.xz

  echo "Extracting..."
  tar xf gimp-api-docs-$VERSION.tar.xz

  echo "Installing tarball data into public/..."
  rm -fr public/api/3.0/*/
  mkdir -p public/api/3.0/

  mv gimp-api-docs-$VERSION/reference/images public/api/3.0/
  mv gimp-api-docs-$VERSION/reference/libgimp-3.0 public/api/3.0/libgimp
  mv gimp-api-docs-$VERSION/reference/libgimpui-3.0 public/api/3.0/libgimpui
  #mv gimp-api-docs-$VERSION/g-ir-docs/html/gjs public/api/3.0/g-ir-docs/
  #mv gimp-api-docs-$VERSION/g-ir-docs/html/python public/api/3.0/g-ir-docs/

  echo "Tidying up..."
  rm -fr gimp-$VERSION gimp-api-docs-$VERSION.tar.bz2

  echo "Done."
  # (2) Dependencies gi-docgen API reference

  DEP=
  APIVER=
  for arg in "${@:2}"
  do
    if [ -z "$DEP" ]; then
      DEP=$arg
    else
      APIVER=$arg
      get_dep_api_docs "$DEP" "$APIVER" "$GITLAB_URL_BASE" "$GITLAB_JOB" "$GITLAB_BROWSE"
      DEP=
      APIVER=
    fi
  done

  # (3) INSTALL file in GIMP source tarball

  echo "Getting GIMP source tarball..."
  wget --no-verbose $DOWNLOAD_URL/gimp-$VERSION.tar.xz
  has_tarball="$?"

  if [ $has_tarball -eq 0 ]; then
    echo "Extracting..."
    tar xf gimp-$VERSION.tar.xz

    echo "Installing tarball data into public/..."
    mkdir -p public/core/setup/build/2.99/
    mv gimp-$VERSION/INSTALL public/core/setup/build/2.99/

    echo "Tidying up..."
    rm -fr gimp-$VERSION gimp-$VERSION.tar.xz
  else
    echo "Tarball not found"
  fi

  echo "Done."
}

function get_dep_api_docs () {
  DEP=$1
  APIVER=$2
  GITLAB_URL_BASE=$3
  GITLAB_JOB=$4
  GITLAB_BROWSE=$5

  DEP_DOWNLOAD_URL="https://download.gimp.org/$DEP/$APIVER"

  echo "Looking up latest $DEP version:"
  MICRO=$(wget --no-verbose -O - $DEP_DOWNLOAD_URL | grep -o "$DEP-$APIVER.\([0-9]*\).tar" |sed "s/$DEP-$APIVER.\([0-9]*\).tar/\1/"|sort --numeric-sort |tail -1)
  DEP_VERSION="$APIVER.$MICRO"
  echo "- Found: $DEP $DEP_VERSION"

  echo "Getting $DEP documentation tarball..."
  TARBALL=$DEP-api-docs-$DEP_VERSION.tar.xz
  wget --no-verbose $DEP_DOWNLOAD_URL/api-docs/$TARBALL

  echo "Extracting $TARBALL..."
  tar xf $TARBALL

  echo "Installing $DEP $DEP_VERSION documentation into public/..."
  rm -fr public/api/$DEP/
  mv $DEP-api-docs-$DEP_VERSION/$DEP-$APIVER public/api/$DEP

  echo "Tidying up..."
  rm -fr $DEP-api-docs-$DEP_VERSION $TARBALL
}

echo "#### GIMP 2.10 data ####"
get_api_docs 2.10
echo "#### GIMP 2.99 data ####"
get_dev_api_docs 2.99 babl 0.1 gegl 0.4

cd public/core/setup/build/
ln -fs 2.10/INSTALL INSTALL
