+++
title = "GIMP Developers Conference 2006"
abbrev = "GIMPCon 2006"
description = "Lyon, France"
author = "The GIMP Development Team"
weight = -4
+++

The GIMP Developers Conference 2006 was held as a sub-event
of the <ulink url="http://www.libregraphicsmeeting.org">Libre
Graphics Meeting</ulink> in Lyon, France, on the 17th, 18th and
19th of March.

{{< figure src="people_small.jpg" link="people.jpg" caption="The People at GIMPCon 2006" >}}

## About the GIMP Developers Conference

The GIMP Developers Conference, also known as GIMPCon, is a
gathering of GIMP and GEGL developers from all over the
World. It is a vital opportunity for GIMP developers to meet
each other and discuss the direction which the software will
take over the coming years.

There have been four GIMP Conferences previously: two in Berlin,
Germany hosted by the CCC (the Chaos Computer Club), one in
Kristiansand, Norway as part of GUADEC 2004 and one in
Stuttgart, Germany as part of GUADEC 2005.

## Minutes of the GIMP Developers Conference 2006

During LGM, we had two sessions dedicated to GIMP: one on Saturday
afternoon (16:30-18:00) and one on Sunday (10:30-16:00). The following
points were on the agenda:

* [GIMP 2.4](#gimp-24)
* [Documentation](#documentation)
* [GIMP 2.99](#gimp-299)
* [How to get more GIMP developers?](#how-can-we-motivate-people-to-join-the-gimp-development)
* [GIMP vision](#gimp-vision)

{{< figure src="meetings_small.jpg" link="meetings.jpg" caption="Discussions and presentations at GIMPCon 2006" >}}

## GIMP 2.4
   
### SIOX
      
Gerald explained the current status of SIOX and its future developments:

* There are still some performance concerns because SIOX was designed
  mainly for web-sized images. In the morning, Raphaël suggested a way
  to add a low resolution preview for large images and this feature will
  be implemented soon.
* Implement a detail refinement brush. Simon suggested to apply the
  refinement automatically on the edge area, but Gerald replied that
  defining such an area assumes that the shape is simple, which is
  something that SIOX tries to avoid.
* New preview toggle, which temporarily disables the automatic
  computation for multiple brush strokes for example.
* A volunteer for integrating more SIOX features in GIMP is needed.

### New tools

* Sven pointed out that the rectangular and ellipse selection tools
  should have the same behavior.
* The new align tool is also confusing for some users. Its behavior is
  still too flaky. Sven mentioned that Peter Sikking may be able to help
  in determining what the users want. A session with Peter is planned
  for tomorrow (the [vision discussion](#gimp-vision)).
* The code for the Lanczos interpolation is currently broken. Either we
  fix the Lanczos code or we hide it from the 2.4 release by removing it
  from the UI and from the PDB.

### Google's Summer of Code

Dave pointed out that the Summer of Code is coming soon and ideas are
needed. GIMP is one of the projects which can offer tasks to be solved.

Ideas and Tasks are needed soon, probably around the beginning of May.

### Preparation for the 2.4 release

Michael pointed out that some terms in the GIMP UI uses are
misleading and very different from other image manipulation
programs. One example is the misleading dpi instead of ppi.

Michael proposed a Texttool PDB and also new features: multiple
styles for a line of text, more control over text.
Unfortunately, GIMP does not currently support all pango
features. Sometimes ligature problems occur with Bitstream
Vera Sans.

Various other issues were also discussed: undo handling of
vector operations, improvements to the full screen mode (some
plug-ins cannot display progress bars), etc.

### TODO

* A list of blocker bugs for the 2.4 release is needed.
* A string and UI freeze is needed.
* Mitch's cleanup should not be commited before the 2.4 release.
* Volunteers are needed for checking GIMP terms (English and translations). A wiki page should track the terms and also possible translations. One volunteer should elaborate if Rosetta from Ubuntu can be used for this.
* A bug is needed for plug-ins which use g_message() and where dialogs pop up behind the main window.
* A volunteer is needed for the Texttool PDB.

## Documentation

The manual currently features nine languages, five are activly
worked on. Russian and Norwegian will be added after the 0.10
release. DocBook/XML is a barrier for new contributors, but there
is currently no valuable alternative available.

For PDF creation we should use dblatex instead of the dead project
db2latex.

The more translation an XML file holds the more difficult it is
for contributors to concentrate on their content. Maybe we should
split up the content directory-wise instead of profiling.

Michael proposed way users can comment on HTML pages like the PHP
documentation. The problems are: what will happen to comments
after an update of the documentation, how and who manages
comments, which system will suit our needs.

### TODO

* Stick to DocBook/XML for the next year. Be aware of the problems and
  change to a different internal structure if needed. A new project page
  should make it easier to contributors to join the project.
* Elaborate which issue tracker can lower the barriers for feedback from
  our readers without turning it into a discussion forum that needs
  active moderation.
 
## GIMP 2.99

### GEGL

Pippin pointed out that GEGL does not yet support calculation
over regions of interest. Some of the code that does that is
currently 8-bits only, but the reference buffer implementation
would be 32bits float per pixel. Yosh proposed to generate all
the code needed for this from a template.

Pippin: GEGL is undead.

Mitch suggested that the integration of GEGL should be in small
steps. The plug-in API should be GEGL only, but will provide
backwards compatibility from old plug-ins for 8bits only.

UI and Usability problems are also caused by the indexed mode.
We concluded that it should be available if the user exports
his image to an indexed format, like GIF or PCX. For some indexed
images, the order of the colors in the palette is significant.
Raphaël proposed to handle these images through GEGL as 16bits
images with the 8 least significant bits representing the
original palette index so that it can be restored later when
saving. Gerald mentioned that Apple may already be doing
something similar.

### TODO

* Write the 32bits float version first, then generate others from that
  reference version.
* Replace the code in the base directory by GEGL operations. First
  replace the image map tool (used for color correction, etc.) using
  GEGL adapters around the pixel buffers. Then later replace the layers
  and masks.

## How can we motivate people to join the GIMP development?

The current project page for gimp.org is not really structured and
not very easy to get in touch with the developers. It would
help to show to the potential contributers a list of tasks that
GIMP developers consider relatively easy and useful for the
next version. We could also use the task list for the Google
Summer of Code or for bounties, etc.

We should make sure that people feel welcome when they read the
GIMP mailing lists. For example, we should avoid criticizing
those who mention GimpShop or commercial programs such as
Photoshop.

Some suggestions were discussed, such as introducing aging in
the list of GIMP authors. The About dialog would first list
those who contributed to the current version and then a longer
list of previous contributers. Sven and Yosh remarked that we
are not really using the mailing lists for discussing GIMP
development. Most of the discussion happens on IRC or directly
between people. This could be improved. There was also some
debate about how to avoid pointless discussions on the gimp-web
mailing list.

### TODO

* Setup a tasks list on the wiki of what can be achieved and
  implemented. It ranges from easy to very complex tasks. Each task
  should be linked to a bugzilla bug report. If someone starts working
  on one of these tasks, its bug report should be marked as ASSIGNED.
* Improve the infrastructure for the web site and try to make
  contributions easier: simpler installation and testing, if possible
  without requiring special skills or permissions.

## GIMP vision

Peter Sikking helped us to formulate a product vision for GIMP.
The vision helps to define what standard installation of the
GIMP is: what plug-ins are standard and what plug-ins are
optional. It shouldn't be a simple cost-and-benefit analysis of
what it means to support a feature. The current problem of the
product vision is too broad to be practical. Peter proposes that
our product vision should address a kind of Gauss curve of what
users GIMP is made for. Currently we have the experts and newbies,
but a low point for the intermediates.

Good defaults would be chosen depending on our vision, not
by inventing new scenarios for each thing that we want to
evaluate. The user scenarios would be written down in advance.
These scenarios should not be changed afterwards because it would
lead to too much discussion. The goal is to cut down all this
discussion. **The product vision is to be used as a filter.**
For example: If someone comes with the request that the UI of GIMP
should be like Photoshop, we can simply state: "We are not trying to be
like Photoshop, because we have a different product vision." Though, the
feature requests should still be examined carefully.

### Targeted user base
      
GIMP targets experienced users. If we acknowledge that GIMP is
not (primarily) for beginners, we cut off a lot of problems
such as "do we need to support that," etc.  Peter noted that a "GIMP
Light" would not just have some options cut off from the menus: it would
have a completely different user interface, even if it would use the
same code under the hood.

Some developers work on GIMP to promote the Free Software
movement and would probably not contribute if GIMP was not
free. Others think that GIMP should provide fun for its
developers, although our user base has grown a bit large for
just doing fun experiments.  We have to acknowledge that we
address a user base that may be more experienced in image
manipulation than we are, so the developers are partially out
of the target group.

Before converging towards a definition of the GIMP target
groups and GIMP vision, there were several discussions
involving examples and use cases, whether GIMP should be the
best image manipulation program in the universe (best for
who?), whether those working on icons and those working on
photos have the same needs (number of images open, relative
sizes), whether people need to switch frequently between GIMP
and other applications (browser or editor for web work),
whether we will support painting with shapes and natural
media, etc.

Eventually, a GIMP vision emerged...

### What GIMP is:

* GIMP is Free Software
* GIMP is a high-end photo manipulation application, and supports creating original art from images;
* GIMP is a high-end application for producing icons, graphical elements of web pages, and art for user interface elements;
* GIMP is a platform for programming cutting edge image processing algorithms, by scientists and artists;
* GIMP is user-configurable to automate repetitive tasks;
* GIMP is easily user-extendable, by easy installation of plug-ins.

### What GIMP is not:

* GIMP is not MS Paint or Adobe Photoshop

### TODO

* Make it easier to perform repetitive tasks (macro recording)
* Provide a UI with a low barrier to entry
* GIMP should be easily extensible by the average user: one click-installation of plug-ins

Minutes written by Roman Joost and Raphaël Quinet. Photos by
Jean + Karine Delvare and Raphaël Quinet.
