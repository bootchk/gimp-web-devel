+++
title = "GIMP Developers Conference 2004"
abbrev = "GIMPCon 2004"
description = "GUADEC, Kristiansand"
author = "The GIMP Development Team"
weight = -3
+++

The GIMP Developers Conference 2004 was held as a sub-event
 of [GUADEC 5](http://2004.guadec.org) in
Kristiansand, Norway, on the 28th, 29th and 30th of June.

## GIMP related talks at GUADEC

### Programming a GIMP Plug-in — Dave Neary

{{< figure src="Dave_Neary.jpg" link="mailto:bolsh@NOSPAM.gimp.org" caption="Dave Neary" >}}

Dave Neary's been hanging around free software for about 6
years. He started off as a leech, and then decided to turn into
a leechee sometime around 1999, when he started working on the
GIMP. His main interest in the GIMP is revitalising the
relationship betweek the user and developer communities. He has
also helped out a little now and again on other things, and is
listed as a co-author of gnect.

He is currently living and working in Lyon, France.

### Custom widgets in GtkCairo — &Oslash;yvind Kol&aring;s

{{< figure src="Oyvind_Kolas.png" link="mailto:pippin@NOSPAM.gimp.org" caption="&Oslash;yvind Kol&aring;s" >}}

&Oslash;yvind is working on an application called Bauxite, which
uses a node-based data structure similar to GEGL This
application is both a standalone video editor and a
comprehensive test bed of GEGL's node structrues.

&Oslash;yvind is also working on the next generation XCF format.

His primary interests in the GIMP is graph compositing.

## Other graphics talks at GUADEC

* Typography and graphic design for programmers —Liam Quin
* The future of rendering in GNOME — Owen Taylor
* GStreamer Internals — Benjamin Otte
* Digital Photography in a GNOME Environment — Hubert Figui&eacute;re

## Sponsors

Many thanks to our generous sponsors, without whom GIMPCon
would not be possible:

{{< figure src="gnu-head-sm.jpg" link="http://www.fsf.org" caption="The Free Software Foundation" >}}

The Free Software Foundation has consistently been the greatest
supporter of the GIMP project, and has been the primary sponsor
of each of the three GIMP Conferences which we have held. Many
thanks go out to RMS and everyone else at GNU.

{{< figure src="mac_gimp_logo4.png" caption="MacGIMP" >}}

Mat Caughron sells the GIMP commercially with support and
documentation, and has been generous on each occasion we have
asked for his support.

The GIMP is also fortunate to have a community of supporters who
have donated smaller amounts to the project. Many thanks go out
to all those supporters. More information on how to donate to
the GIMP is available on <ulink
url="http://www.gimp.org/donating/">the GIMP home page</ulink>.

## GIMP conference activities

The GIMP Developers Conference, also known as GIMPCon, is a
gathering of GIMP and GEGL developers from all over the
World. It is a vital opportunity for the GIMP developers to meet
each other and discuss the direction which the software will
take over the coming years.

There have been two GIMP Conferences previously, both in Berlin,
and both graciously hosted by the CCC (the Chaos Computer Club),
a much less chaotic bunch of guys than their name suggests.

## The GIMP Developers Conference 2003

{{< figure src="../2003/people-small.png" link="../2003/people.png" caption="The People at GimpCon 2003" >}}

Last year's GIMP Conference was a sub-event of the Chaos
Communications Camp, a camping trip with broadband and
laptops. During the camp, we discussed ways we could make the
GIMP better both technically and as a community, which led up to
the 2.0 release at the end of March.

At this year's conference we will discuss, among other things,

* What we expect from a replacement for the PDB (Procedural Database)
* How we will integrate GEGL into the GIMP
* Managing plug-in distribution
* The GIMP's organisation

## The GIMP, Live!

Two of our resident graphics designers, Jakub Steiner (jimmac)
and Tuomas Kuosmanen (tigert) are giving a user-day GIMP
demonstration on Wednesday. Last year, jimmac presented a series
of tutorials on basic GIMP techniques to a crowd of over 100
people, and held them captivated for nearly 3 hours.

This is an opportunity to see what the GIMP can do in the hands
of professionals, and to pick up some tips for your own use. A
spectacle not to be missed.

## About the GIMP

Started in 1995 by Spencer Kimball and Peter Mattis, the GIMP
has evolved into a mature and powerful application. The current
release is version 2.0, which has major improvements over its
predecessors in the user interface, making the GIMP more
accessible for casual users as well as graphics professionals.

{{< figure src="macosx_screenshot1_thumb.jpg" caption="GIMP on the Mac OS X operating system" >}}

GIMP is expandable and extensible. It is designed to be
augmented with plug-ins and extensions which allow access to its
capabilties via a number of languages. The advanced scripting
interface allows everything from the simplest task to the most
complex image manipulation procedures to be easily scripted.

The software comes with lots of features, some are newly
implemented and available with the 2.0 Release. A short list
over the most interesting features are:

* a full suite of tools for painting and image manipulation
* preview of brush outline while you're drawing
* customizable interface using a drag-and-drop dockable
* advanced scripting capabilities (Scheme, Python, Perl)
* over 100 plug-ins available as standard
* fullscreen mode and view filters
* much, much more!

* [The main GUADEC website](http://2004.guadec.org)
* [Getting there](http://www.hia.no/oksam/inf-vit/travel.php3)
* [Accommodation ](http://www.hia.no/oksam/inf-vit/accomodation.php3)
