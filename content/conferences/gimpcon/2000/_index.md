+++
title = "GIMP Developers Conference 2000"
author = "The GIMP Development Team"
abbrev = "GIMPCon 2000"
description = "Chaos Computer Club, Berlin"
date = "2000-06-02"
weight = -1
+++

The first official GIMP Developers Conference took place
*June 2nd - 4th 2000* in [Berlin](https://www.berlin.de).

The [Chaos Computer Club Berlin](http://berlin.ccc.de/)
was so kind to allow us to use their rooms for the
conference. The [Chaos Computer Club](https://www.ccc.de)
is a galactic community of human beings including
all ages, genders, races and social positions. They demand
unlimited freedom and flow of information without censorship.

![](people-small.jpg)

From left to right, top to bottom: Calvin, Simon, Seth, Jakub,
Lauri, Austin, Tigert, Tim, Jens, Tor, Jay, Daniel, Sven, Adam,
Mitch, Garry, Marc, Caroline, Andy, Yosh.

We'd like to thank the [Free Software Foundation](https://www.fsf.org),
the [Chaos Computer Club](https://www.ccc.de) and
[O'Reilly Germany](https://www.oreilly.de) for their
help that made this meeting possible.

