---
title: "Libre Graphics 2011: Montréal, Canada"
author: "The GIMP Development Team"
date: 2011-05-10
lastmod: 2016-10-24
---

* [Homepage](http://libregraphicsmeeting.org/2011/)
* [Program](http://libre-graphics-meeting.org/2011/program/)
* [Video podcast](https://itunes.apple.com/de/podcast/lgm-2011-video/id450990980)


## GIMP and GEGL topics

* Michael Terry: Introducing AdaptableGIMP [Blog](http://adaptablegimp.blogspot.de/)
* Nicolas Robidoux: Better and faster image resizing and resampling [Description](http://libre-graphics-meeting.org/2011/day-4#NicolasRobidoux)

## Other topics

Colour management, other tools etc.
