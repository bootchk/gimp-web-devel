---
title: "Libre Graphics 2007: Montréal, Canada"
author: "The GIMP Development Team"
date: 2007-05-04
lastmod: 2016-10-24
---

* [Homepage](http://libregraphicsmeeting.org/2007/)
* [Program](http://libregraphicsmeeting.org/2007/program.html)
* [Audio podcast feed](http://libregraphicsmeeting.org/2007/audio.rss)
* [Video podcast feed](http://libregraphicsmeeting.org/2007/video.rss)


## GIMP and GEGL topics

* Judith Beaudoin: A GIMP overview
* Peter Sikking and Kamila Giedrojć: OpenUsability - Project overview and first results from the OpenUsability GIMP redesign project
* Karine Delvare: Contributing to an Open Source project, taking GIMP as an example.
* Prof. Michael Terry: Ingimp: An instrumented version of GIMP to automatically collect usability data
* Cédric Gemy: Working with Open Standards and FLOSS (Inkscape, Gimp, Blender, Scribus)
* Jakub Steiner: Photo management and editing with F-Spot and GIMP
* Øyvind Kolås: GEGL – a graph based image processing and compositing engine. A presentation of capabilities, data model, public API and an overview of opportunities for internal optimisations and enhancements

## Other topics

* Benoit St-André: Contributing without coding
* Harrisson and Femke Snelting: Relaying Systems – Why designers should be interested in FLOSS
* Hubert Figuière: Digital Photography with libre Software: RAW, metadata, workflow and asset management
* Alexandre Robin: Our first year of graphic design ... 100 % open source
* Ted Gould: Introduction to the SVG format
* Cyrille Berger: OpenRaster: a new file format to fully share complex raster graphics between applications
* Nicolas Spalinger: The Open Font License, purpose, achievements, future
* David Maxwell: Static Analysis results on graphics software

Other topics were open standards, XSL-FO, Inkscape, Blender, Scribus, Krita, sK1, OpenClipart etc.
