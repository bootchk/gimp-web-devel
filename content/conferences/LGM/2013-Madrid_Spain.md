---
title: "Libre Graphics 2013: Madrid, Spain"
author: "The GIMP Development Team"
date: 2013-04-10
lastmod: 2016-10-24
---

## Links

* [Homepage](http://libregraphicsmeeting.org/2013/)
* [Program](http://libregraphicsmeeting.org/2013/program/)
* [Video archive](http://medialab-prado.es/article/lgm_2013)

## GIMP and GEGL topics

* GIMP team: Save your work, export results^W^W^W^W^W^WQ+A with GIMPers. [presentation](http://medialab-prado.es/article/gimpers)
* Steve Czajka: GIMP Magazine
* David Tschumperlé: G'MIC: A full-featured framework for image processing with various interfaces
* Camille Bissuel, Cedric Gémy, Alexandre Quessy: Mikado, a "Future tool" for image manipulation with graphs and nodes based on Tempi and GEGL

## Birds of a feather session

### Participants

* Jakub Steiner            (jimmac)
* Øyvind Kolås            (pippin)
* Ville Pätsi            (drc)
* Simon Budig            (nomis)
* Michael Natterer        (mitch)
* Michael Schumacher        (schumaml)
* Jon Nordby            (jonnor)
* Jehan Pagès            (jehan)
* Tobias Jakobs
* Pablo Lopez
* Aryeom Han
* James Prichard
* Roberto Roch
* + 2

### Topics

#### G’MIC & a GEGLified GIMP

Pippin describes how G’MIC can be changed to add its filters to GIMP 2.10 and up, this extends to how this can be done in general. It is mentioned that plug-ins have to use the new plug-in API, or they will never get access to floating point data etc.

What G’MIC has to add is a way for GIMP to query the available operations and their parameters in order to build generic interfaces for them (GParam* is mentioned)

#### Symbolic Icon Theme

Jimmac talks about the symbolic icon theme that has been created during GNOME Outreach Program for Women. This will most likely be used for GIMP 3.0

#### Knowledge transfer to forums

schumaml mentions that overall, many replies given to questions of users in forums about gimp are not really satisfactory - sometimes even when being given by regulars and administrators of the respective forums. This is seen as an area where non-coding contributors can do a lot of good work - by interacting in forums on the one hand, but also by providing top-notch tutorials, which will act as reference for GIMP usage on the other hand.

#### Malicious downloads - how to fight them

There are many places where GIMP, and especially its Windows installers, can be downloaded from. Some of these are places we like to do so - Jernej, Simone, Partha, Photocomix, other GIMP forums.

Small bias against big downloader sites is there is suspicion that they might be repackaging the installers.

Growing(?) problem of sites hiding behind “green download arrow” ad banners which lead users to InstallIQ-infested Windows installers for GIMP. Limited problem with outright trojans masquerading as GIMP, c.f. [https://mail.gnome.org/archives/gimp-developer-list/2012-August/msg00074.html](https://mail.gnome.org/archives/gimp-developer-list/2012-August/msg00074.html)

No real solutions, but an attempt can be made to get Google and other search engines to delist those sites.

Constructive suggestion by Jimmac: prevent users from having to search for GIMP by getting it into OS X and Windows app stores, problem with that is subscriptions for developer certificates.

#### Google Summer of Code

We are out of easy tasks to do, the remainder is complex.

Suggestions for projects to offer/look out for:

* PSD format support improvement for loading, concentrating on retaining the image structure and correctness of the visual result. Exporting to PSD won’t result in the same file, even if nothing has changed.
* Slicing tool
* Resurrecting mathmap
* Running gegl ops out of process, like GIMP does with its plug-ins. Concept is defined, requires hard-core gobject knowledge.
* babl optimization, will require near obsessive-disorder determination to lower the errors, but also communication and social skill which are considered normal by established societies

#### Interjection by two local designers

How is animation support in GIMP, and will it improve?

Unfortunately not, but we explained how the existing, albeit cumbersome layer-as-frame animation works, and mentioned GAP.

Discussion diverted to inclusion of Frame Manager from Cinepaint (infamous inside joke: “CEASE AND DESIST”), and that it isn’t really for animation, rather frame-by-frame video retouching.

#### How to deal with abusive people on lists, Bugzilla, ...

Recently, we’ve had to deal with one person on GNOME Bugzilla, who can, by all standards, be described as abusive - even repeated requests to change the behaviour have been unsuccessful.

We do not want to establish a formal policy about how to deal with such behaviour, if only for not encouraging people to “test” it by navigating along its loopholes. Incidents will be evaluated on a case by cases basis, on the assumption that 99,9xxxx% of all people communicating in the project will never violate any of it.

If GNOME establishes a policy for Bugzilla (and it seems like they’re doing so), we will evaluate and respond to it.

Our goal is to be as inclusive as possible, but not on the expense of other participants.
