---
title: "IRC developer meeting — 2011 March 28"
author: "The GIMP Development Team"
date: 2011-03-28
lastmod: 2021-04-17
---

Meeting page for the Developer Meeting which will take place in the GIMP IRC on March 28th 2011, 10:00 PM CET (For time zone conversions, see [here](http://www.timeanddate.com/worldclock/fixedtime.html?month=3&day=28&year=2011&hour=22&min=0&sec=0&p1=48))

**Time for Next Meeting(?):** April 14 2011, 10:00 PM CET

## Agenda

### GSoC Student Applications

Official GSoC applications should start coming in on march 28 19:00 utc - that's 2 hours before the meeting should begin. Also, we already have student applications on the mailing list. Some suggestions were made on putting minimal student requirements (not sure how practical this is) and some suggested creating some quick start guides for new developers. Do we want to do anything to get students started?
BTW, since this is the developer wiki, such content should be placed here!

Rating of students should be done after April 8 (student application deadline) and finish on April 22, so this can be done on the next meeting.

For obvious reason, the writer of these lines (who applies for GSoC himself) will not participate in that part of the discussion.

### Optional topic: Re-Discussing GIMP's programming language

Some developers that weren't present in the last meeting, highly disagree on the attempt to introduce vala into gimp, claiming that it will scare off developers (more than the "simple" C GObject code). '''Please see the topic thread on the mailing list!'''

We should seperate this for:

* GUI - Maybe try to use GtkBuilder and friends in more places, instead of a new language?
* Core

Also, we should discuss the reason for such act (GObject glue code is "annoying", or something else?)

### Default JPEG Quality

As someone suggested on irc a week ago, we should probably raise the default quality for jpeg images in gimp. Disk space and bandwidth are bigger today than before, and the current default quality is rather noisy.
Hopefully just a yes/no vote, nothing more than that (unless there are some other plugins which have quality parameters).

### bringing UI crew to LGM

UI team gets build as we speak, can they come to LGM. would be good.

### Resource management for 2.8

Where are we with new brushes, patterns, etc.?

### Anything else?

Tell LightningIsMyName and he'll add it :)

## Decided Actions

* GSoC Student Applications
  * Core developers sign up as mentors (for GSoC)
  * schumaml: Write mail about application is open, and required skills, and required code example
* Re-Discussing GIMP's programming language
  * For core (non-UI), continue using GObject, use code generators (such as [turbine](http://git.gnome.org/browse/turbine/)) and do copy/paste/replace for existing GObject classes (for the rare case where the generator won't be enough).
  * For UI, porting widgets to GtkBuilder is an option (and then we'll use Glade and UI xml files), but any action on the UI is postponed until we get 3.0 out!
* Default JPEG Quality (quickie, not a real topic)
  * Change to 95 2x1, and add a hack to save defaults (using something like the PNG plugin does, or something more elegant). Note that a decent system to save plugin defaults, along with other api changes, is not something that will happen for 2.8.
* bringing UI crew to LGM
  * Will be discussed with mitch and schumaml on the financial aspect (using gimp funds) when the team is fully assembled (parts of it are already assembled - hurray for guiguru and congrats for the new team memebers!)
* Resource management for 2.8
  * One member of the new UI team (lead by guiguru) will take care of that

## Meeting Log

No propper meeting logs, since the discussion included lots of off-topic stuff, and many of that should be filtered...
Important off-topic points from the meeting, in addition to the decided actions above:

* Seems as if most GIMP team can't attend LGM this year, but there may be a GIMP village in the [Chaos Communication Camp](http://events.ccc.de/2010/08/10/chaos-communication-camp-2011/) as most developers want to attend it
* UI team members will start hanging on IRC once the team is assembled. Many developers suggested help in setting them up with build environments and every other thing they need. This will happen soon, so be nice to the new guys ;)
