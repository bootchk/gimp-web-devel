---
title: "IRC developer meeting — 2011 February 28"
author: "The GIMP Development Team"
date: 2011-02-28
lastmod: 2020-01-26
---

Meeting page for the Developer Meeting which took place in the GIMP IRC on February 28th 2011, 10:00 PM CET (GMT+1)

**Time for Next Meeting:** March 14 2011, 10:00 PM CET (GMT+1).

## Agenda

### GSoC 2011 - ORG Application deadline is in 11 days!

We need to discuss the list of ideas that were raised on the mailing list. See [http://old.nabble.com/Google-Summer-of-Code-2011---Project-Ideas-Suggestions-to30911798.html](http://old.nabble.com/Google-Summer-of-Code-2011---Project-Ideas-Suggestions-to30911798.html)
for GEGL, see [http://gegl.org/contribute.html](http://gegl.org/contribute.html)


### Bug fixing priorities

We won't get 2.8 or 3.0 out unless we start  doing concentrated bug fixes. We should set a priority list and try to stick with it, even if working on other stuff is more fun.


### Future development?

Some serious PDB changes are going on now, potentially many of these will be relevant for 3.0. This is worth a discussion, but not necessaraly today.


### Make an official wiki working again (i.e. make wiki.gimp.org pointing at it)!

Can be done by making [http://gimp-wiki.who.ee/](http://gimp-wiki.who.ee/) the official wiki - we need it to apply for GSoC, write roadmaps, etc. With all respect for bugzilla, discussions should be done in some more apropriate page.


### Make a roadmap on the official wiki

Self explanatory.


### Try and decide on clear release policy for beta builds

I personally would like to see more recent beta releases, and the last one was 7 months ago!


### Schedule meetings?

Depending on the success of this one, we should possibly start doing this regularly. This was tried in the past, and we'll try to do this again.


### Anything else?

Tell LightningIsMyName and he'll add it :)

Random thought: Add an option to support gegl plugins with a gui nicely, for 2.8, so that users will start developing plugins for 3.0 in GEGL?


## Decided Actions

* schumaml and LightningIsMyName fix wgo?
* Alexia and mitch take care of redirecting wiki.gimp.org
* Enselic makes a draft of a GIMP roadmap and sends to gimp-developer
* everybody subscribe to bug mail and TRIAGE BUGS
* mitch, make a development release soo
* LightningIsMyName takes care of next meeting
* setting pages on the wiki to discuss changes in different topics
* LightningIsMyName gets a list of projects on the wiki by tomorrow, developers review the ideas by email or by commenting on the wiki. Saturday (March 5th, 2011) is hard dead line for finalizing the project list!
* all devs PM/Email LightningIsMyName username and email

## Meeting Log

```html
## Topic: GIMP developer meeting, 22:00 CET (GMT+1) February 28th 2011 | Agenda: http://gimp-wiki.who.ee/index.php/Hacking:Dev_Meeting_28_Feb_2011 | THIS MEETING IS BEING LOGGED!

<lightningismyname> Let's start :) Please take a look at the Agenda, as pointed by the topic, and read it
<lightningismyname> The most burning issue for todays meeting, is potentially GSoC this year
 We have 10 days to apply, and we still don't have an organized ideas list or a mentors list
 Do we want to participate?
<xsdg> I would suggest that the bigger issue is one of overall organization, and that the GSoC issues and the release issues are a consequence of that
<lightningismyname> xsdg, you are probably right, but I don't know if we have time to fix all these before the application deadline
<xsdg> As far as GSoC itself, if mentors want to step forward, great.  I have neither the knowledge nor the time.  I'm not sure there's anything non-mentors can really do about it
 we could potentially lower the bar for mentoring, but that's probably not such a great idea.  It'd probably be more valuable (if mentors don't step forward) to deal with the higher-level issues and think about it again next year
<lightningismyname> mitch, I know you offered to mentor "since you do it anyway". Now, is it serious and you have time, or was that just a cynical comment?
<mitch> i meant it, but i'm not going to mentor a particular student
 i will help when i'm on irc
 mitch gives channel operator status to Mikachu
<mitch> we should hear schumaml on that issue, he's been doing gsoc orga all the years
<lightningismyname> schumaml - your opinion?
<schumaml> I won't force anyone to become a mentor
<mitch> so my estimate is that if nobody steps up and organizes things, gsoc this year will not happen
<schumaml> if some of the previous mentors want to do it again, then I'll take care of the orga again
<mitch> Alexia wanted to mentor again afail
<lightningismyname> schumaml, does GSoC require specific mentors for each project, or can we have general mentors?
<mitch> specific mentors
<schumaml> each student is assigned one mentor
<schumaml> and this mentor is responsible for all administrative stuff concerning that one student
 e.g. evaluations
<lightningismyname> Taking a look at the list of projects, does anyone have something which is urgent and GSoC could probably help more than regular development?
<lightningismyname> I.e. is there something a student can take care of which is critical?
<schumaml> should we decide not to apply as an org, then mentors could approach gnome ans ask to mentor gimp or gegl-related tasks
<mitch> not really, otherwise it wouldn't be critical :)
<guiguru_> (switching machine)
<lightningismyname> Let's jump for a second - what's on the top of the todo list?
 Except for fixing bugs
<mitch> generally getting 2.8 out
<lightningismyname> I can't see a clear roadmap anywhere, and that prevents me from knowing what to work on
<schumaml> I'd like to have an auto-updating main website again
<mitch> schumaml: you just volunteered
<enselic> we should switch to a wiki so it's not a big project to keep the website up to date
<lightningismyname> I volunteer to try and do website fixes if needed, the question is what do we want from auto-updating website?
<mitch> a wiki for the main site?
<lightningismyname> It was discussed previously today to point wiki.gimp.org at alexia's wiki. Any objections?
<enselic> most parts of the main site
<mitch> i don't think so, both for reasons of load and security, as pointed out on the list
<enselic> not news for example
<mitch> LightningIsMyName: not from me
<schumaml> I think Alexia got a point about not introducing dynamic systems to www.gimp.org
<enselic> we could have non-free-for-all registration
<mitch> Enselic: giving out accounts is a prerequisite
<lightningismyname> The registration to alexia's wiki is aprooved only by me and her, to avoid spam
<mitch> no self registration
<schumaml> we discussed that earlier today - as long as there is any script involved, the site is exploitable
<enselic> LightningIsMyName: could I get an account?
<lightningismyname> Each dev who creates an account (see the topic) and email me, will get an editing permission on the wiki
<schumaml> and believe me, www.gimp.org would be a priority one target
<lightningismyname> who has the control over the domain names?
<mitch> snorfle
<lightningismyname> snorfle?
<mitch> shawn admunson(?)
* Kevin waves hello
<mitch> so let's focus in wgo for a moment
 schumaml: since you see it as priority, what about mailing sven about an account, and simply fixing it? sven doesn't have the time
<schumaml> I don't know what is missing, all I know is that commits to gimp-web aren't pulled and shown within minutes
 I don't know if I can fix it, but I'
<kevin> Sounds like you need Jenkins for the wgo
<schumaml> ll try to do this
<mitch> i don't know either, but sven can point you to a starting point
<lightningismyname> I volunteer to try and help on that, if I'll have an account on gimp.org
<mitch> fine with me
<mitch> so action #1: schumaml and LightningIsMyName fix wgo?
<lightningismyname> fine with me
<schumaml> yes
<kevin> If there is a script to build the wgo pages from a repo it just needs to be run by a cron job as a simple way to fix the problem.
<lightningismyname> question 2: who can request re-direction of wiki.gimp.org to alexia's wiki?
<mitch> i think the problem was commit hooks or something that would send a mail from the old svn server to foo and then bar would happen
<kevin> mitch: That would be the better way instead of a cron job
<mitch> Kevin: that's how it worked before
* LightningIsMyName points back at redirecting wiki.gimp.org
<mitch> LightningIsMyName: ideally, that wiki would be moved to the machine gimp.org runs on
<schumaml> I think we can add a daily cron job as a fallback at least
<kevin> who currently owns/maintains the DNS entries for g.o?
<mitch> it's really big enough
 Kevin: as said, snorfle
<lightningismyname> didn't we just say we don't want that for security reasons?
<mitch> LightningIsMyName: it could run in a chroot, or a vm, as sven said
<lightningismyname> so we'll need someone who can mess with servers more than I know to do that
<schumaml> I guess we need emergency response plans anyway?
<lightningismyname> unless we have someone who volunteers to do that, that's ain't practical
<mitch> but for the time being, a redirect would be better than the current situation
<lightningismyname> agree
<enselic> +1
<schumaml> i.e. what to do if somethings seems fishy with any gimp.org?
<enselic> mitch: can you talk to snorfle about that?
<kevin> I have no problem working on servers
 snorfle?
<enselic> Kevin: gimp.org DNS admin
<xsdg> Kevin: snorfle is a person
<pippin> Shawn Amundson?
<mitch> Enselic: sure, if alexia provides me with details, i'll take care of that
<mitch> action #2: Alexia and mitch take care of redirecting wiki.gimp.org
 pippin: precisely
<lightningismyname> mitch, the link is at the topic, alexia is back, just joined #gimp
<kevin> yes, I see Shawn. Snorfle has some interesting meanings in the Urban dictionary
<enselic> Ok, next topic "Make a roadmap on the official wiki"
<lightningismyname> Big topic
<enselic> I can take care of that
 I just need an account
<lightningismyname> we first of all need 2.8 goals asap
 Enselic, create one now, I'll give your permissions
<enselic> that's easy, look at https://bugzilla.gnome.org/buglist.cgi?product=GIMP&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED⌖_milestone=2.8 and work on what's important ;)
<mitch> LightningIsMyName: that would basically be "fix all milestome bugs" and "clean up open ends"
<mitch> it's not *that* much to do actually
<lightningismyname> guiguru, are you here? Some bugs there are dependant on you
<guiguru> I guess so
<enselic> LightningIsMyName: how do I register?
<lightningismyname> guiguru: what are the chances we can start seeing specs soon?
 Enselic, one sec
<enselic> LightningIsMyName: specs are not blocking us, we'll do our best if we don't get specs
<enselic> we can't wait forever
 but we can fix things afterwards forever
<lightningismyname> Enselic, http://gimp-wiki.who.ee/index.php?title=Special:UserLogin&amp;type=signup
<guiguru> LightningIsMyName: high
<mitch> and spec &lt;-&gt; impl is always a loop, so just get started
<kevin> Apart from SF deprecation issues, there are two minor enhancements it would be nice to have in there before 2.8 (getenv and create a directory procedures)
<enselic> LightningIsMyName: priv msg
<xsdg> Enselic: are the priorities on that list accurate?
<enselic> xsdg: no
<xsdg> Enselic: how hard would it be for them to become accurate?
 that would be a big help, I think
<mitch> bugzilla priorities are noise only for the most part
<xsdg> (I've certainly looked at that list and not known what to focus on)
<enselic> xsdg: you can pick any of them
<mitch> xsdg: just pick *any* bug that seems doable
<xsdg> are they all 2.8 blockers?
<mitch> even non-milestone bugs, as long as the patch fixes something :)
<enselic> no, some of them we will ship without
<xsdg> Enselic: it would be useful to know the difference between the two classes, then
<enselic> I'm going to clean that list up once I get my site up and running
<kevin> xsdg: The higher priority ones would be ones with 2.8 as their target milestone
<xsdg> Kevin: all of the things on that list are milestone 2.8
* LightningIsMyName points at "Random thought: Add an option to support gegl plugins with a gui nicely, for 2.8, so that users will start developing plugins for 3.0 in GEGL?"
<kevin> I'm not sure which list you are looking at
<lightningismyname> is this practical?
<kevin> Oh, that list.
<mitch> and really, evetybody can help by simply subscribing to bugmail and at least *triaging* new bugs
<enselic> action #3, Enselic makes a draft of a GIMP roadmap and sends to gimp-developer
<mitch> come on guys, it's not that hard
<enselic> next item on the agenda: "Try and decide on clear release policy for beta builds"
<kevin> Sure. Just mark them all as RESOLVED, WONTFIX  ;-)
<xsdg> mitch, Enselic: if our first goal is to get 2.8 out the door, we need to focus on the things which will allow us to accomplish that
<mitch> how did that agenda point come up?
<pippin> LightningIsMyName: that is part of the random non-roadmap which is http://gegl.org/contribute.html
<enselic> I don't know, but I think we should have nightly binary builds so we don't need to worry about that
<mitch> oh "beta build" is what we call a devel release now
<xsdg> mitch: regarding subscribing to bugmail, would be good for there to be a way for people to learn about that
<enselic> we have nightly tarball builds, it's not that hard to make binary builds from that
<mitch> i can make one easily within the next 2 weeks or so
 now that gegl is out
<lightningismyname> I can have nightly builds of gtk+babl+gegl, but I need some FTP to host them
 +gimp
<mitch> Enselic: imho that can exist and is good, but advertizing it too much will feed the trolls
<enselic> LightningIsMyName: host them here ftp://gimptest.flamingtext.com/pub/nightly-tarballs/
<schumaml> how will we deal with nightly builds in bugzilla?
<enselic> mitch: "feed the trolls"?
 how?
<lightningismyname> Enselic, need to check with ftcameron. I'll email him to see it's OK
<mitch> a lot of "testing" from the clueless, and even more useless bugs
<enselic> LightningIsMyName: you don't need to, I am admin on that machine
<xsdg> schumaml: I think, possibly as a separate action item somewhere, we need to spend a bit more time making bugzilla more useful
<schumaml> bug reported against 2.7.x-git20110316
<mitch> schumaml: precisely
<enselic> mitch: or more useful bugs
<xsdg> schumaml: that would likely include updating the templates, making sure the defaults are useful, and not letting people edit the ones they tend to enter incorrectly
<mitch> Enselic: nobody is even triaging new bugs around here
 so action 3: everybody subscribe to bug mail and TRIAGE BUGS
<michaelgr> If I may interject, nightly builds might also attract developers, if you advertise it.
<enselic> it's not that important, is it?
<xsdg> mitch: are there any guidelines for triaging bugs?
 Enselic: "it" -&gt; ???
<mitch> xsdg: look at them, understand them, judge them
 Enselic: triaging bugs?
<xsdg> mitch: what does that mean?
<enselic> unimportant bugs that is
<kevin> xsdg: Read them, does it make sense, can the problem be reproduced, does it need more input from reporter
<schumaml> xsdg: it's not that hard, most are either reproducable or needinfo
<lightningismyname> I have a document on making useful bug reports, is it possible to link to it in the submission form
 ?
<mikachu> Enselic: deciding which are important == triaging :)
<enselic> I ignore == I think it's not important
<schumaml> or duplicates
 lots of duplicates
<mitch> xsdg: check for duplicates, ask questions and set to NEEDINFO, close useless crap right away
<xsdg> mitch, Kevin, schumaml: that's more than I knew 10 minutes ago.  As obvious as this may seem, these instructions need to go somewhere.
<kevin> Right. also checking if bug has already been reported.
<xsdg> it doesn't help if they only exist in your heads
<schumaml> xsdg: gnome has bug triaging guidelines
<jonnor> gnome should have general documentation for triaging available
<kevin> If a patch has been supplied, does it look right, does it apply to the current (git master) source tree.
<mitch> xsdg: get a wiki account and start a page :)
<enselic> anyway.... action #4: mitch, make a development release soon
<mitch> Enselic: fine with me
<xsdg> mitch: I'm currently at work; not going to happen
<enselic> mitch:
 mitch: thanks
 next item on the agenda "Schedule meetings?"
<lightningismyname> Enselic, sure! I want to learn to make a release, I'm going to be increasingly active this year
<mitch> LightningIsMyName: there is a release-howto in devel-docs
<lightningismyname> The agenda isn't exactly ordered
<kevin> LightningIsMyName: make distcheck is a start
<lightningismyname> I think there are some other things which should be discussed
<enselic> make distcheck is run each night...
<schumaml> meetings? yes. late, like 10pm. interval?
<lightningismyname> the question is if we want to get techinical today or some other time?
<mitch> Enselic: action #5: enselic to finally return ;)
<lightningismyname> I think that a bi-weekly meeting cycle would be productive
<enselic> I'm afraid my girlfriend has infected my mind, I see a future of less GIMP activity :/
<mitch> LightningIsMyName: you are more than welcome to push that
<enselic> but it's a good kind of infection
<lightningismyname> mitch - I am trying :)
<enselic> I like being infected
<mitch> Enselic: turn her into a hacker
<schumaml> mitch: haeckse
<enselic> meeting: not that often, every 2 months?
<kevin> Definitely a good type of infection to have
<lightningismyname> Also, I'm going to add another topic which I saw in comments of some users
 They said we should sort of advertise gimp, by promoting the new features officially instead of just developer blogs
<schumaml> two months is too long, imo
<kevin> if you want regular meetings someone will have to set an agenda for the meetiings
* pippin would like to have the meetings in #gimp instead of here
<mitch> LightningIsMyName: if wgo was working, much of that would happen
<lightningismyname> show more activity on the main website, possibly with features videos, to attract devs
<mitch> pippin: i disagree
<xsdg> Let's all pause; I think everyone's talking about a different thing
 What are we discussing right now?
<enselic> pippin just want the pleasure of telling people to shut up
<mitch> heh
<enselic> right now the topic is "Schedule meetings?"
<lightningismyname> Boys, calm down ;) we were talking about scheduling meetings
<mitch> LightningIsMyName: you seem to do a pretty good job, just go ahead with regular meetings
<enselic> mitch: +1
* pippin already has &gt;30 tabs in his irc client, would prefer not to have more
<mikachu> you can close this one soon
<kevin> 30??
<lightningismyname> We need to get some technical meetings, and not just one like these
* LightningIsMyName reminds people to stay on topic
<lightningismyname> some people objected the recent PDB refactors
 and there are some more changes that need to be dicussed
<schumaml> technical meetings can be done by groups of interested people
<mitch> LightningIsMyName: this is a good start already, we can'T get technical with hald our infrastructure down
<enselic> action #6: LightningIsMyName takes care of next meeting
<pippin> I like how the blenders devs hijack their regular channel, use /topic to force people staying on topic stating which part of the agenda they are on, etc. :)
<mitch> half*
 pippin: offtopic
<kevin> :-)
<pippin> mitch: it is on topic on the topic of scheduling meetings
<lightningismyname> action #7: setting pages to discuss changes in different topics
<enselic> next item on agenda: "Anything else? "
<prokoudine_> LightningIsMyName, promoting features officially is not a problem
<lightningismyname> prokoudine_, you volunteer?
<prokoudine_> LightningIsMyName, I write news for gimp.org anyway
<lightningismyname> good to know - the news are anonymous :)
<mitch> LightningIsMyName: will you put the log and the collected actions up in the wiki?
<lightningismyname> yes. btw, we still haven't finished one topic
<kevin> Are you only going to promote new features after they are complete?
<mitch> i know :)
<lightningismyname> We didn't decide a thing about GSoC
<lightningismyname> do we apply this year for something?
 I agree to be a student, and there will be probably many other applications
 there will be student interest, so it's a shame to miss the coding work
<prokoudine_> LightningIsMyName, and will you mentor yourself? :)
* pippin does not want to have direct responsibility of any student, wbut will aid in helping any strudent working on gegl or gegl related projects
<schumaml> Alexia would mentor, I guess
 Kevin ketas-wi
<lightningismyname> ketas-wi, you are Alexia, right?
<mitch> LightningIsMyName: i would mentor you :) but not some random fool
<schumaml> I've asked Akkana as well, she could do a beginner task provided that it will end up in gimp if finished
<prokoudine_> LightningIsMyName, he is not
<gwidion> btw, sorry for last years gsoc.
<kevin> I took myself off the mentor list but I could sign up again if there was something I might be able to help with. Last couple of years there hasn't been.
<prokoudine_> gwidion, just finish the project, will you? :)
<gwidion> my student would do even less things than what i 'd  ask her
<mitch> LightningIsMyName: which would be self-mentring, but google's formalities followed, and i'd be around for any questions of course
<lightningismyname> is there any project which we want to feed me/some-other-jobless-student for the summer?
<kevin> Rewrite gimp-perl  :-)
<pippin> LightningIsMyName: see http://gegl.org/contribute.html
<mitch> fix pygimp
<jonnor> If Enselic does not expect to find more time for gimp dev soon, having someone else work on single-window mode is pretty critical for 2.8
<lightningismyname> jonnor, you are very right about that
<jonnor> it is afaik, the one big thing blocking it
<ankh> time-critical things have not been good for gsoc projects in the past
<mitch> jonnor: i'm afraid that particular job will stick with me anyway
<ankh> jonnor: that and more-than-8-bits
<jonnor> Ankh: that is not for 2.8?
<schumaml> can we release 2.8 with gtk+ 2.x at all?
<mitch> Ankh: that's not a gcos project
<lightningismyname> Anything potentially 3.0 targeted, that will not need re-writing later?
<xsdg> Question: do we have a time goal for shipping 2.8?  I would say "december 31, 2011 at the very latest"
<mitch> gsoc
<ankh> mitch: agreed
<kevin> mitch: Anything that can be taken off your plate and pushed to someone else to free up your time for SWM?
<mitch> so, i have a general issue now that 3.0 came up
<ankh> neither swm nor ?8bit is gsoc I fear
<mitch> i figured that we will likely *never* get proper xinput back with gtk 2.x
 so might i suggest something:
<schumaml> and tablet support is broken on windows, too
<mitch> 2.x -&gt; 3.x migration:
<kevin> I'm also curious why ruler widgets were removed from gtk
<gwidion> prokoudine_: My intentio is to "finish" the other Gsoc - jsut gimp python which was never integrated into trunk.
<mitch> - deprecate as much as humanly possible in the 2.x api and replace it by stuff that is 3.x-able
<enselic> xsdg: we don't know, but will know within a few months
<mitch> - after 2.8 is out, have a short devel cycle
<prokoudine_> gwidion, that's what I was referring to -- python project
<carol> not /src/gimp/gkt+ like gap and ffmpeg? moving targets and all....
<mitch> - gfet 3.0 out
<lightningismyname> schumaml, does a ORG need to supply a list of projects in his application? If so, does google choose between these, or the ORG?
<pippin> LightningIsMyName: the _student_ has to propose his own project, potentially inspired by the orgs list
<xsdg> Enselic: I would suggest that we set a hard deadline sometime, and commit to shipping by that date.  Otherwise we end up having this discussion next March
<enselic> xsdg: that's not going to work
<schumaml> mitch: so 3.0 would be just the gtk+ change?
<kevin> Part of the release date issue is having a defined set of things to work on and getting those things done.
<mitch> schumaml: plus really nicely working tablet hotplug and stuff
 schumaml: and it's a big change
<lightningismyname> schumaml, would you take care of our GSoC application this year? I'll organize a list of projects, and I'll put it on for review by the devs? Link will be on the mailing list tomorrow
<schumaml> s/just //
<lightningismyname> btw, all devs that want a wiki account, PM me a username and an email
<xsdg> Enselic: Suppose that GIMP hadn't advanced March of next year.  Should we not release the stuff that's already done by that point?
<xsdg> Enselic: what's in git already has value over the latest thing that's been released
 (that is, excluding dev releeases)
<xsdg> Enselic: do we want people to sit on 2.6 forever?
<kevin> oh, well.
 I still have 2.6 on my machine.
<schumaml> xsdg: if they need tablet support, then probably yes
<enselic> xsdg: the harm is done, people expect swm, we will get a lot of badwill if we ship without
<mitch> xsdg: then just get started doing some hacks, roadmaps and hard dates don't help much on their own
<gwidion> shure
<xsdg> Enselic: is that worth people sitting on 2.6 for another year?
<lightningismyname> I want to see 2.8 out soon, it's pretty stable at least for my everyday workflow. Better release and stop blocking ourselves, than blocking ourselves and the users
 So it will a bit incomplete - legitimate for an end of cycle product
<xsdg> mitch: I've been on a project that died because they couldn't release (geeqie).  I'm hoping it doesn't happen to gimp
<pippin> Enselic: you just have to take a week off from work and emma, and hide in a rødmalt stuga...
<prokoudine_> Enselic, what is exactly blocking simple swm?
<kevin> One user reported working in SWM mode and felt it was working ok.
<schumaml> LightningIsMyName: yes, please collect a list of projects
<prokoudine_> Enselic, without image parade, I mean
<enselic> see milestoned bugs...
<lightningismyname> prokoudine_, it's not session managed
<schumaml> LightningIsMyName: google uses this to judge whether an org is ready for gsoc
<prokoudine_> LightningIsMyName, any estimations time-wise?
<mitch> prokoudine_: what is the problem about image parade? i could hack that in one week
<prokoudine_> mitch, no spec?
<kevin> image parade?
<mitch> prokoudine_: not a problem, i have a mind
<prokoudine_> mitch, then go for it? :)
<xsdg> mitch: this is why we need priorities
<kevin> mitch is supposed to be working on SWM  ;-)
<prokoudine_> that will leave us only broken tablets
<mitch> prokoudine_: if you noticed, i'm already the main committer and my time is limited ;)
<prokoudine_> mitch, I have :) I track it
* pippin thinks mitch should focus on killing off 8bit :P
<mitch> and other swm things are far worse
<lightningismyname> action #8: LightningIsMyName gets a list of projects on the wiki by tomorrow, developers review the ideas by email or by commenting on the wiki. Saturday is hard dead line for finalizing the project list
<schumaml> do we have any unassigned action items?
<mitch> pippin: irrelevant for 2.8
<enselic> mitch: btw, at some point you need to stop rebasing the gtk3 branch and merge master to it instead
 regularly
<pippin> "gimp developer(s) continue to ignore higher bitdepths" ;)
<mitch> Enselic: clearly not
* LightningIsMyName points at action #8
<mikachu> i don't think that's a meeting topic
<xsdg> schumaml: I would suggest that we have people initially prioritize things on the 2.8 milestone
<mitch> i want a sane history
<enselic> mitch: how are more than you supposed to be able to work on it then?
<gwidion> pippin: "which sounds better than gimp developers continue not releasing"
<xsdg> schumaml: I'm willing to help with that, but I don't feel I know enough to pass judgment on _all_ of those bugs
<gwidion> unfortunatelly
<mitch> Enselic: i don't see anyone even trying ot
 it
<lightningismyname> any devs will be willing to review the idea list with me?
<enselic> mitch: no one can until you stop rebasing :)
<mitch> Enselic: and it's close to done anyway
<pippin> 60min since meeting start, where in the agenda are we by now?
<xsdg> that said, I need to run. back in a bit
<lightningismyname> pippin, sort of finished :P
<enselic> mitch: do we share the same definition of "done"?
 done to me means it's ready to be merged to git master and released
<mitch> Enselic: done as in "works better than master" &lt;- good enough? :)
<enselic> if there are no known regressions, than it's good enough
 then*
<mitch> there are missing plugins still
 but Mikachu is very helpful there
<mikachu> is neo still working on gfig?
<mitch> Mikachu: damn good question
* Enselic -&gt; bed
<lightningismyname> Summarizing the list of actions:
 action #1: schumaml and LightningIsMyName fix wgo?
 action #2: Alexia and mitch take care of redirecting wiki.gimp.org
 action #3, Enselic makes a draft of a GIMP roadmap and sends to gimp-developer
<ankh> neither swm nor ?8bit is gsoc I fear
 *8
 `
<lightningismyname> action 4: everybody subscribe to bug mail and TRIAGE BUGS
  anyway.... action #4: mitch, make a development release soo
 action #6: LightningIsMyName takes care of next meeting
 action #7: setting pages to discuss changes in different topics
 action #8: LightningIsMyName gets a list of projects on the wiki by
  action #8: LightningIsMyName gets a list of projects on the wiki by tomorrow, developers review the ideas by email or by commenting on the wiki. Saturday is hard dead line for finalizing the project list
<mitch> very nice
<mikachu> you missed five
<prokoudine_> LightningIsMyName, what happened to 5?
<mikachu> LightningIsMyName: if i can help with some git thing wrt updating the web site, just ask
<lightningismyname> action #9: all devs PM me username and password
 sorry, an email
 not a password
 MikeM Mikachu
 Mikachu, thanks :)
<kevin> prokoudine_: It was the second #4?
<mitch> LightningIsMyName: thanks for taking care of meeting stuff!
<lightningismyname> mitch, no problem :) I just hope next time we get actual stuff done in addition to talking :)
 schumaml, and everyone, I'll email the project list page to the dev-mail-list, people should review the list!
<kevin> Is there an overall tracking bug for SWM stuff?
<bat`o> is cage tool in 2.8 or not a topic that's worth to discuss ?
<lightningismyname> There will be a wiki page for each important milestone, and I'll bug devs to fill the pages on their stuff
<mitch> Bat`O: of course it will be in 2.8
<bat`o> \o/
<xsdg> kevin, I don't really think so
<mitch> Bat`O: what about showing your changes done since it was merged?
* Bat`O need to get back to work
<xsdg> kevin, maybe just "do SWM", but certainly nothing beyond that
<bat`o> mitch: you mean, merge my branch to master ?
<lightningismyname> Anyone has any objection to lock the official meeting?
<mitch> Bat`O: no, getting it past my evil review
<kevin> I see 5 reports re: SWM
<bat`o> i'm not against
 but there is still a bad bug i need to kill
<xsdg> kevin, I could be wrong :o)
<mitch> i want one patch to try to apply
<xsdg> It'd be awesome if I were
<mitch> one patch where i can clearly see the changes, not some evil git diff against a non-rebased branch
<kevin> xsdg: I think you might be right that there isn't a tracking bug.
```
