---
title: "Maintainer Corner"
date: 2022-10-18
author: Jehan
weight: 1000
show_subs: false
---

This is the **Maintainer Corner** in the wider sense, i.e. any person who is in
charge of some aspect of GIMP infrastructure.

Here, we will gather info on how to perform our tasks efficiently.

## GIMP Maintainers

GIMP maintainers have a few more responsibilities than most contributors, in
particular regarding releases and coordination.

Some of these duties include:

- [Setting the version of GIMP as well as the API version](versioning/)
- [Maintaining the AppStream metadata](appstream/)
- [Making releases](release/)
- Managing dependencies: except for core projects (such as `babl` and
  `GEGL`), we should stay as conservative as possible for the stable
  branch (otherwise distributions might end up getting stuck providing
  very old GIMP versions). On development builds, we should verify any
  mandatory dependency is at the very least available in [Debian
  testing](https://www.debian.org/distrib/packages),
  [MSYS2](https://packages.msys2.org/package/) and
  [MacPorts](https://ports.macports.org/). We may be a bit more adventurous for
  optional dependencies yet stay reasonable (a feature is not so useful if
  nobody can build it). In any case, any dependency bump must be carefully
  weighed within reason, especially when getting close to make the development
  branch into the new stable branch. See also
  [os-support.txt](https://gitlab.gnome.org/GNOME/gimp/-/blob/master/devel-docs/os-support.txt).
- Manage other contributors, [give developer rights](giving-developer-right) to
  the Gitlab project, etc.
- Maintain [milestones](gitlab-milestones/)
- Maintain our [roadmaps](/core/roadmap/)
- Maintain `NEWS` file in GIMP repository. Any developer is actually encouraged
  to update it when they do noteworthy changes, but this is the maintainers'
  role to do the finale checks and make sure we don't miss anything. One purpose
  of this file is to make it as easy as possible to write release notes based on
  this file rather than reviewing hundreds of commits.

Current GIMP maintainers are [Jehan](https://gitlab.gnome.org/Jehan) and
[mitch](https://gitlab.gnome.org/mitch).

## Other maintainance roles

Other types of responsibilities exist, the [accounts managers](accounts/) page
list contributors with various maintenance or administrative roles.
