---
title: "Third-party service accounts managers"
date: 2022-10-18
author: Jehan
---

We will be listing here which contributors have access to which account. We only
consider an internet venue as "official" if there is at least one member from
the core community (which we consider trustworthy because they created ties with
the rest of the team, usually through contributions, nice behaviour towards
others, helping others…) with moderation access.

## Community
### IRC

Our channels are on GIMPNet, the IRC server originally created for the GIMP
project.

Our `#gimp` channel owner is:

* drc

Channel admins are:

* nomis
* schumaml

Channel ops are:

* ender
* mitch

### Social accounts

Social accounts all use the same email. Any attempt to use another login email
should be considered an attack attempt and should be processed as soon as
possible to block the attempt.

Mastodon
: * demib0y/ankh (in-progress)
  * Jehan

Twitter
: * Jehan
  * schumaml

Facebook
: * demib0y/ankh
  * patdavid

### Forums

Pixls.us Discourse
: * patdavid

GNOME Discourse
: Exceptionally, we don't have anyone from the GIMP team with an admin/moderator
hat on this instance. GNOME is maintaining this space for us. For any issue,
people to contact would be ebassi (Discourse admin), av/AndreaVeri or barthalion
(GNOME Infrastructure team). They are usually available on the `#sysadmin`
channel on GIMPNet or by opening a topic under "*Site Feedback*" on the Discourse
instance itself.

Discord
: * demib0y/ankh
  * Sevenix

### Videos

Peertube
: The GIMP video channel is maintained by Jehan. Currently Peertube only accepts
one owner/moderator (until the feature for multi-management of a channel is
implemented).

Youtube
: We have a "GIMP" brand account. Current primary owner is Jehan, Managers are
Alexia and patdavid. Communications manager is Americo.

## Infrastructure
### Windows Partner Center

This is for publishing our .msixupload files on the Microsoft Store. For this account, we
use a dedicated alias too.

People with access to GIMP account are:

* ender
* Jehan

In particular, if someone needs their own access, ask ender. He is the one who
knows how to give access.

### GNOME OpenShift

A platform handling websites and other hosting stuff (e.g. MirrorBits for
download mirrors). People with access to the "gimp" project are:

* Jehan (role: admin)
* pippin (role: edit)
* schumaml (role: admin)
* Wormnest (role: edit)

### GNOME master server

Where we publish to the download server. People with access are (possibly more
exist but unsure if others still have access):

* Jehan
* mitch
* pippin
* schumaml

### CircleCI

Unlike the <abbr title="Continuous Integration">CI</abbr> for Linux and Windows,
which uses GNOME Gitlab runners, we use CircleCI for macOS builds. CircleCI
requires the project to be on Github, which is why we have a Github mirror of
the `gimp-macos-build` repository (normally, `Infrastructure/` repositories are
not mirrored, but GNOME infrastructure team made a special case for this one).

To get access to the settings of our CircleCI account, you must actually get
access to the `GNOME/gimp-macos-build` Github repository (CircleCI doesn't have
its own access settings, it mirrors the ones from the Github repository).

Current members with Admin access on this Github mirror, hence CircleCI:

* Jehan
* lukaso
