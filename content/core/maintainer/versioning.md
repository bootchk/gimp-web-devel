---
title: "Versioning GIMP"
date: 2014-08-27
author: Mukund Sivaraman
---

This is a brief description of how the various version variables at the
top of `meson.build` are to be set, including `gimp_interface_age`.

## Software version

The software version is composed of 3 version numbers: a major version, followed
by a minor version, followed by a micro version (dot-separated):
`<major>.<minor>.<micro>`.

The **major version** so far changes with GIMP's Toolkit
([GTK](https://gtk.org/)) version: we incremented to version 3 when we ported to
GTK+3. We will be at GIMP 4 when we will port to GTK4.

**Stable release versions always have even minor and micro versions**. Odd minor
version is a development branch for the next even minor version. Odd micro
version is git unreleased code. *Development release's version* are composed of
an odd minor version with an even micro version.

From GIMP 3.0.0 and onward, our versioning scheme for minor and micro numbers of
**stable releases** is as follows:

* **New features or functional changes should imply a minor version bump**. The
  micro version is reset to zero.
* **Bugfix-only releases imply a micro version bump only.**

In other words, we get back to the versioning scheme up to GIMP 2.8 branch,
which we had [relaxed in the 2.10
series](https://www.gimp.org/release-notes/gimp-2.10.html#roadmap-and-whats-next).
Nevertheless we don't want anymore the situation we had back then where people
would wait years for a new minor version. We are now targetting smaller and more
focused minor releases so that we can have a minor-point release every few
months.

## Library versions
### API version

Our libraries are suffixed with `<major>.0` (e.g. `3.0` for the whole GIMP 3
major series), whatever the minor and micro versions.

In particular, it means that our main `libgimp` symlink will be `libgimp-3.0.so`
and the `pkg-config` module is called `gimp-3.0`. The actual version of the API
though is the same as the [software version](#software-version). For instance,
libgimp shipped with GIMP 3.2.4 can be verified with:

```sh
$ pkg-config --modversion gimp-3.0
3.2.4
```

The main consequence is that backwards-compatibility is promised within a major
series

Example
: A plug-in created for GIMP 3.2.4 should continue working on any further
release of GIMP 3, with same minor version and higher micro version (e.g. GIMP
3.2.10) or with higher minor version (e.g. GIMP 3.4.0).

### Libtool version

See [Libtool’s versioning
system](https://www.gnu.org/software/libtool/manual/html_node/Libtool-versioning.html)
in GNU docs, for definitions of libtool "current", "revision" and "age" numbers
as used below. This versioning scheme is separate from the main software and API
version and allows to encode in the shared library file names additional
concepts such as when new functions have been added.

1. When making releases on the **stable** branch, increment the micro version
   and interface age:
   ```C
   gimp_micro_version += 1;
   gimp_interface_age += 1;
   ```

   The micro version is the third number in the `version` field of `project()`
   meson method. `gimp_interface_age` is its own variable.

   If `gimp_binary_age` is defined as a constant, set `gimp_binary_age += 1` else
   if `gimp_binary_age` is computed (see last point about backwards incompatible
   library updates), leave it as it is.

2. **NEVER** increment `gimp_interface_age` by more than 1 at any time for
   whatever reason. If you increment `gimp_interface_age` by more than 1, then
   the libtool "current" number is decremented which could result in
   incompatible library interface with existing bin programs.

3. **If any functions have been added**, set `gimp_interface_age=0`. This will
   cause the "current" and "age" part of libtool version to bump upwards,
   increasing the interface number the library implements while keeping the
   minimum interface number supported the same as before (i.e., backwards
   compatible ABI).

   Example 1
   : In GIMP 2.8.10, with `gimp_minor_version=8`, `gimp_micro_version=10` and
   `gimp_interface_age=10` (incremented by 1 for every micro release),
   `current=800` and `age=800`, which means that the libraries support interface
   numbers 0 through 800, and the interface DID NOT change at all between GIMP
   2.8.0 to GIMP 2.8.10.

   Example 2
   : In GIMP 2.8.12, with `gimp_minor_version=8`, `gimp_micro_version=12` and
   `gimp_interface_age=0`, `current=812` and `age=812`, which means that the
   libraries support interface numbers 0 through 812, and the ABI interface DID
   change in backwards compatible manner at the time `gimp_interface_age` was
   set to 0.

4. If backwards compatibility was broken, set `gimp_binary_age=0` and
   `gimp_interface_age=0`.

   This will cause "age" part of libtool version to be 0, increasing the minimum
   interface supported to "current" part of libtool version, and making ABI
   backwards incompatible (the linker will not be able to use these libraries
   with programs compiled to work against older libraries).

   ⚠️ Note that **normally this case should not happen within a given stable
   series** if possible. The ideal case should simply be a major version bump,
   so the minor and micro versions will be back to 0, which will trigger the
   interface age to be reset to 0 as well (using the default definition
   `gimp_binary_age = 100 * gimp_app_version_minor + gimp_app_version_micro`).

   Example
   : In GIMP 2.8.14, with `gimp_minor_version=8`, `gimp_micro_version=14`,
   `gimp_binary_age=0` and `gimp_interface_age=0`, `current=814` and `age=0`,
   which means that the libraries support interface number 814 only, which tells
   libtool the ABI interface changed in backwards incompatible manner at the
   time `gimp_binary_age` was set to 0.
