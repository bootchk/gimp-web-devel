---
title: "Setting up your developer environment"
date: 2022-08-07
author: "Jehan"
abbrev: "dev-env-setup"
weight: 1
---

The first step as a core developer is to build GIMP. This page contains
all the resources necessary to set up your developer environment
correctly.
