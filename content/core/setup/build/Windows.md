---
title: "Building GIMP for Windows"
Author: "GIMP team"
Date: 2021-08-30
weight: 3
---

The main GIMP building instructions are at [Building GIMP](../). This page is for Windows-specific additions.

## Supported Windows API

The supported versions of Windows are noted in the
`devel-docs/os-support.txt` file in the respective branches:

* [OS support for GIMP 2.10 (stable
  branch)](https://gitlab.gnome.org/GNOME/gimp/-/blob/gimp-2-10/devel-docs/os-support.txt)
* [OS support for GIMP 2.99 (development
  branch)](https://gitlab.gnome.org/GNOME/gimp/-/blob/master/devel-docs/os-support.txt)

GIMP project is favoring backward compatibility as a general rule and we really
don't like deprecating hardware (unfortunately often associated to OS version,
sometimes for no good reasons, sometimes on purpose by vendors) when it is just
a few years old. Nevertheless we may have to bump our Windows requirement when
it becomes too hard to maintain both old and new <abbr title="Application
Programming Interface">API</abbr>s.

## Building GIMP natively under Windows using MSYS2

MSYS2 is a POSIX environment with basically everything you would find on
a Linux system (shell, package manager, compiler…). It allows to use POSIX
tools to build Windows software.

**Running, from GIMP source, `build/windows/1_build-deps-msys2.sh` then
`build/windows/2_build-gimp-msys2.sh` might be all there is to do**.
To better understand the compilation flow, you may read below.

### Install the dependencies for GIMP

1. First, follow the installation instructions for MSYS2 available at
[msys2.github.io](https://msys2.github.io/) or simply run in PowerShell
(Win 10 and +): `winget install MSYS2.MSYS2`.
2. To open a terminal, you need to execute `CLANGARM64` (for ARM 64-bit,
unstable branch only), `CLANG64` (x86 64-bit) or `MINGW32` (x86 32-bit)
(you can find these shortcuts on the Start Menu).
3. Update the system:

```sh
 pacman -Syyuu
```

4. If the shell tells you to close the terminal, close it and run again. That is a normal procedure.

5. Dependencies are installed with the following command:

#### Stable branch (GIMP 2.10) dependencies

```sh
pacman -S --needed \
    git \
    base-devel \
    ${MINGW_PACKAGE_PREFIX}-toolchain \
    ${MINGW_PACKAGE_PREFIX}-asciidoc \
    ${MINGW_PACKAGE_PREFIX}-drmingw \
    ${MINGW_PACKAGE_PREFIX}-gexiv2 \
    ${MINGW_PACKAGE_PREFIX}-gettext-tools \
    ${MINGW_PACKAGE_PREFIX}-ghostscript \
    ${MINGW_PACKAGE_PREFIX}-glib-networking \
    ${MINGW_PACKAGE_PREFIX}-graphviz \
    ${MINGW_PACKAGE_PREFIX}-gtk2 \
    ${MINGW_PACKAGE_PREFIX}-gobject-introspection \
    ${MINGW_PACKAGE_PREFIX}-iso-codes \
    ${MINGW_PACKAGE_PREFIX}-json-c \
    ${MINGW_PACKAGE_PREFIX}-json-glib \
    ${MINGW_PACKAGE_PREFIX}-lcms2 \
    ${MINGW_PACKAGE_PREFIX}-lensfun \
    ${MINGW_PACKAGE_PREFIX}-libheif \
    ${MINGW_PACKAGE_PREFIX}-libraw \
    ${MINGW_PACKAGE_PREFIX}-libspiro \
    ${MINGW_PACKAGE_PREFIX}-libwebp \
    ${MINGW_PACKAGE_PREFIX}-libwmf \
    ${MINGW_PACKAGE_PREFIX}-meson \
    ${MINGW_PACKAGE_PREFIX}-mypaint-brushes \
    ${MINGW_PACKAGE_PREFIX}-openexr \
    ${MINGW_PACKAGE_PREFIX}-poppler \
    ${MINGW_PACKAGE_PREFIX}-python2-pygtk \
    ${MINGW_PACKAGE_PREFIX}-SDL2 \
    ${MINGW_PACKAGE_PREFIX}-suitesparse \
    ${MINGW_PACKAGE_PREFIX}-xpm-nox
```

Note that $MINGW_PACKAGE_PREFIX is defined internally by MSYS2. You don't need
to manually change it.

#### Unstable branch (GIMP 2.99) dependencies

```sh
pacman -S --needed                          \
          git                               \
          base-devel                        \
          ${MINGW_PACKAGE_PREFIX}-toolchain \
          $(cat build/windows/all-deps-uni.txt                           |
            sed "s/\${MINGW_PACKAGE_PREFIX}-/${MINGW_PACKAGE_PREFIX}-/g" |
            sed 's/\\//g')
```

Then simply hit enter at the prompts for which packages to install (default=all).
This step will download a ton of packages, and may take a while.

### Building the software

You can now just follow the instruction on the main page [Building GIMP](../).
Just be careful of the following:

Since you are using two prefixes (the gimp and msys one), disable the
relocatability of GIMP code:

* meson build : `-Drelocatable-bundle=no`
* autotools build : `--disable-relocatable-bundle`

🚧 [babl dev docs are broken](https://gitlab.gnome.org/GNOME/babl/-/issues/97).
Disable them while calling meson: `-Dwith-docs=false`.

🚧 [GIMP dev docs are broken](https://gitlab.gnome.org/GNOME/gimp/-/issues/11200).
Disable them while calling meson: `-Dg-ir-doc=false`; or autotools: `--disable-docs`.

🚧 [Introspection on ARM is broken](https://gitlab.gnome.org/GNOME/gimp/-/issues/11424).
We suggest to disabling it like in crosssroad scripts linked [below](#cross-compiling-gimp-under-unix-using-crossroad).

Once built, you can run GIMP through the arch-specific shell of MSYS. To open via
Windows Terminal, you need to add the prefixes to PATH in 'Environment Variables'.

## Building GIMP using Microsoft tools

Microsoft Visual Studio comes with its own C compiler know as MSVC.
Most of GIMP development is done with `Clang` or `GCC` compilers
(provided by MSYS2).

🚧 [GIMP do not build on MSVC](https://gitlab.gnome.org/GNOME/gimp/-/issues/11473).
If you wish to contribute fixes to make GIMP build with Microsoft VS, then
maintain the build so that it continues working, you are welcome to contribute.

## Cross-Compiling GIMP under UNIX using crossroad

A tool named [crossroad](https://gitlab.freedesktop.org/crossroad/crossroad) has
been developed to cross-build for Windows under Linux (it was even originally
started to crossbuild GIMP specifically and is used until today in our CI builds).

We recommend to check out the scripts `build/windows/1_build-deps-crossroad.sh` and
`build/windows/2_build-gimp-crossroad.sh` for more details on how crossroad works.

🚧 [crossbuilds do not support introspection](https://gitlab.gnome.org/GNOME/gimp/-/issues/6393).
Because GObject-Introspection build (which allows bindings for plug-ins, 
such as Lua, Python, Script-Fu, Vala) is very hard when cross-compiling so
it is usually disabled as the only feature exception when cross-compiling.
Since introspection is needed to run Python scripts at build time, you will
need to build GIMP natively on UNIX before crossbuilding. Patches are welcome.

## Packaging third-party GIMP plug-ins for Windows

Users on the Windows and macOS platforms expect software to be
packaged in a self-installing file. Even though GIMP plug-ins are
relatively easy to install, you might want to package them using an
installer such as [Inno Setup](http://www.jrsoftware.org/isinfo.php)
for the traditional version, or [Windows SDK](/resource/windows-plugin-packaging) for the Store version.
