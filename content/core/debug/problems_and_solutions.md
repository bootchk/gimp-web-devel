---
title: "Common development problems and solutions"
Author: "GIMP team"
Date: 2021-08-18
weight: 1
---

This page describes problems/solutions that hamper GIMP developers. It's a knowledge base for developers.
Feel free to add your own findings and don't be shy. It makes the work easier for all of us.

## Build

### General

#### Libraries versions too low

Problem
: I’m getting an error about a too low of GTK/GLib/*other* while compiling XXX

Solution
: You can either update your version of GTK/GLib using your system’s
package manager, or compile them from source! Compiling these is done
exactly like we compiled `babl` – download the source (either the latest
from `git`, or a package from the official site), compile and install.

#### Searching the package name for a dependency

Problem
: The configuration step complains about a missing dependency but I
don't know how to find the right package name.

Solution
: There are 2 main cases, but they are similarly handled. Basically you
need to find the name of a file which will be available in the package.

The 2 common cases are:

1. The dependency is a library.
    * In the `meson.build` file, it will be searched by a call similar
      to `dependency('gdk-pixbuf-2.0', version: '>='+gdk_pixbuf_minver)`.
      The important part is the first argument here. Since library
      dependency are usually searched by `pkg-config` (it's not
      mandatory, but it's definitely what we favor), the file you want
      to search is the first argument followed by `.pc`. E.g. in the
      given example, you will want to search for a package containing a
      file `gdk-pixbuf-2.0.pc`.  On
    * In the `configure.ac`, it will look like the below code instead.
      Here the important part is the second argument. The rest is the
      same.
```sh
PKG_CHECK_MODULES(GDK_PIXBUF, gdk-pixbuf-2.0 >= gdk_pixbuf_required_version,,
                  [add_deps_error([gdk-pixbuf-2.0 >= gdk_pixbuf_required_version])])

```
2. The dependency is an executable.
    * In `meson.build`, you will see `find_program('perl5', 'perl', 'perl5.005', 'perl5.004', 'perl')`.
      The binary you want to search is any from the list.
    * In `configure.ac`, you will see `AC_PATH_PROGS(PERL,perl5 perl,perl)`.
      The binay will be one from the second argument list.

Now that you have determined what file to search, here are how to find
which package contains it:

Debian and Debian-based distributions
: Install the `apt-file` package. Before the first run, you will need to
run `apt-file update`, then search with `apt-file search perl5` or
`apt-file search gdk-pixbuf-2.0.pc`.

Fedora and dnf using distributions
: Search the package with `dnf repoquery whatprovides */perl5` or `dnf repoquery whatprovides */gdk-pixbuf-2.0.pc`.
Unlike `apt-file`, this command requires exact path. So when you only
know a partial name, use glob patterns.

Windows compilation from Linux using Crossroad
: Inside your crossroad environment, search with `crossroad search --search-files perl5`
or `crossroad search --search-files gdk-pixbuf-2.0.pc`.

We welcome more helper commands for other platforms.

#### Error while loading shared libraries: No such file or directory

Problem
: I've built a shared library. However, building fails with the message `error while loading shared libraries: libmylibrary.so.1: cannot open shared object file: No such file or directory`

Cause
: The linker doesn't find your library, because it's not in a standard location (such as `/usr/lib`).

Solution
: Set the environment variable `LD_LIBRARY_PATH` to the directory which contains your compiled library.

Example
: Given the library is in `/home/username/build/mylibrary/lib` then type:

```sh
export LD_LIBRARY_PATH=/home/username/build/mylibrary/lib:${LD_LIBRARY_PATH}
```

… and try again!

#### No rule to make target 'all'

Problem
: I've created a new subdirectory and the `Makefile.am` file there. However, each time the `make` command stops at this directory with the error `No rule to make target all`.

Cause
: The directory isn't listed among `AC_CONFIG_FILES` in the
`configure.ac` file. Therefore the generated makefile doesn't contain
the target 'all' and the `make` command doesn't know what to do.

Solution
: * Open `configure.ac` in the project's root directory.
  * Search for the line `AC_CONFIG_FILES`.
  * In the following list look for a suitable line to place your new directory item. The line after the parent directory, taking care for the alphabetical order, is a good candidate.
  * Save `configure.ac`.
  * Open a terminal window and change to the directory where you build the project.
  * Run `autogen.sh --prefix=$your_prefix`

#### Make install fails with 'cannot install to a directory'...

Problem
: `make install` fails with `error:cannot install ... to a directory not ending in ...`

You get error messages like this

```sh
make[3]: Entering directory `/home/$your_username/build/gegl-master/operations/core'
/usr/bin/install -c -d /home/$your_username/gimp-install/gimp-feature2/lib/gegl-0.3
for i in clone.la convert-format.la crop.la nop.la; do \
	  /bin/bash ../../libtool --mode=install /usr/bin/install -c $i /home/$your_username/gimp-install/gimp-feature2/lib/gegl-0.3 ; \
	done
libtool: install: error: cannot install `clone.la' to a directory not ending in /home/$your_username/gimp-install/gimp-master/lib/gegl-0.3
libtool: install: error: cannot install `convert-format.la' to a directory not ending in /home/$your_username/gimp-install/gimp-master/lib/gegl-0.3
libtool: install: error: cannot install `crop.la' to a directory not ending in /home/$your_username/gimp-install/gimp-master/lib/gegl-0.3
libtool: install: error: cannot install `nop.la' to a directory not ending in /home/$your_username/gimp-install/gimp-master/lib/gegl-0.3
make[3]: *** [install-exec-local] Error 1
make[3]: Leaving directory `/home/$your_username/build/gegl-master/operations/core'
make[2]: *** [install-am] Error 2
make[2]: Leaving directory `/home/$your_username/build/gegl-master/operations/core'
make[1]: *** [install-recursive] Error 1
make[1]: Leaving directory `/home/$your_username/build/gegl-master/operations'
make: *** [install-recursive] Error 1
```

Cause
: In a former step you've built into another prefix. Somehow the newer build reads remains of that in the aforementioned `*.la` files and stumbles over them.

Solution
: The solution describes the process using the example of GEGL.

* Change into the build directory of that previous build (in the given example this is  `/home/$your_username/build/gegl-master/operations/core`, or into its root folder, here: `/home/$your_username/build/gegl-master`)
* Run `make clean`.
* Set your `INSTALL_PREFIX` variable properly to the folder where you want GEGL install in.
* Change into the folder where you build GEGL from (if you're not already there).
* Re-run `configure --prefix=$INSTALL_PREFIX` (or if you checked GEGL out from Git: `autogen.sh --prefix=$INSTALL_PREFIX && make && make install`

#### Absolute DESTDIR required

If you have weird errors at installation, they may be due to `DESTDIR` being a relative path.

### babl

#### Unit tests fail

Problem
: Unit tests pass on one machine, but fail on another - equally configured - machine.

In this particular case the tests RGBA-&gt;HSLA and RGBA-&gt;HSVA passed on an amd64 platform, but failed on an i386 platform.

This is the content of file tests/testsuite.log on the i386 machine:

```sh
#### ===============================
babl 0.1.11: tests/test-suite.log
#### ===============================
# TOTAL: 19
# PASS:  17
# SKIP:  0
# XFAIL: 0
# FAIL:  2
# XPASS: 0
# ERROR: 0

.. contents:: :depth: 2
FAIL: hsl
#### =
rgba to hsla  failed #0[0]  got 0.166667 expected 0.000000
rgba to hsla  failed #1[0]  got 0.666667 expected 0.000000
FAIL: hsva
#### =
rgba to hsva  failed #0[0]  got 0.166667 expected 0.000000
rgba to hsva  failed #1[0]  got 0.166667 expected 0.000000
```

Cause
: The tested code doesn't take into account that strict equality
comparisons (`==`) will most often lead to different results on
different platforms.

Solution
: The code must take this fact into account. This means: don't use `==` for floating point comparisons!

The following lines show alternatives.

1. Make sure to have defined `EPSILON` first:

```C
#define EPSILON 1.0e-10
```

2. Use `EPSILON` instead of `==` :


| wrong | right |
| ----- | ----- |
| `if (value == 0.0)` |  `if (value < EPSILON)`
| `if (value1 == value2)` |  `if (value1 - value2 < EPSILON)` or `if (fabs (value1 - value2)) < EPSILON` |

See also [Bugfix for RGBA-&gt;HSLA,HSVA conversions](https://git.gnome.org/browse/babl/commit/?id=da3b41274052c23b44ad8eceb9d99106d2739f3a).


Make sure, your code works on all architectures. On a 64 bit machine this would mean also to compile and run `make check` with `CFLAGS="-O2 -mfpmath=387"`.

If you're unsure, come to IRC (channels <a class="external text" href="irc://irc.gimp.org/gimp" rel="nofollow">#gimp</a> or <a class="external text" href="irc://irc.gimp.org/gegl" rel="nofollow">#gegl</a>) and ask.


### GEGL

#### GEGL build breaks for failing GLib assertions

Problem
: Building GEGL master stops with failing assertions related to GLib. The stopped build can't be aborted with Ctrl+C to enter new commands in that terminal session.

The following messages are shown at the screen:

```sh
CC       gegl-matrix.lo
CCLD     libgegl-0.3.la
GISCAN Gegl-0.3.gir
GLib-GObject-CRITICAL **: gtype.c:2720: You forgot to call g_type_init()
GLib-GObject-CRITICAL **: g_type_interface_add_prerequisite: assertion `G_TYPE_IS_INTERFACE (interface_type)' failed
GLib-CRITICAL **: g_once_init_leave: assertion `result != 0' failed
GLib-GObject-CRITICAL **: gtype.c:2720: You forgot to call g_type_init()
GLib-CRITICAL **: g_once_init_leave: assertion `result != 0' failed
GLib-GObject-CRITICAL **: gtype.c:2720: You forgot to call g_type_init()
GLib-CRITICAL **: g_once_init_leave: assertion `result != 0' failed
```

Cause
: You have an old GLib version in your prefix. (Don't be fooled by just looking at the libglib version in your package manager!)

Solution
: * Close this terminal session and open a new one.
  * Remove that old GLib version from your prefix and rebuild GEGL from scratch.

#### SPIRO not found

Problem
: I have SPIRO (packages libspiro0, libspiro-dev) installed. However, at the end GEGLs configure step tells me, that no usable SPIRO was found.

GEGL builds and works anyway, because it is only an optional dependency.

Cause
: The package of your distribution doesn't contain the link
`libspiro.so` to the actual SPIRO library. On a Debian testing system
this concerns the package libspiro0.

Solution
: You have to set that link yourself.
This bug is already known to the responsible [Debian package managers](http://lists.alioth.debian.org/pipermail/pkg-fonts-devel/2013-September/012940.html).

On an i386 platform:

* `cd /usr/lib/i386-linux-gnu`
* Find out the actual libspiro file: `ls -l libspiro.*` The actual libspiro file is a plain file, not a symbolic link, and has a name similar to `libspiro.so.0.0.1`
* Set the link: `sudo ln -s libspiro.so.0.0.1 libspiro.so`. You will have to enter your password for this.
* Point your `LDFLAGS` variable to that directory: `export LDFLAGS="${LDFLAGS} -L/usr/lib/i386-linux-gnu"`
* Point your `LD_LIBRARY_PATH` variable to that directory: `export LD_LIBRARY_PATH="/usr/lib/i386-linux-gnu:${LD_LIBRARY_PATH}"`

If you're working on an AMD64 platform, replace `i386-linux-gnu` by `x86_64-linux-gnu`.

#### GEGL build breaks: 'recipe for target 'Gegl-0.3.gir' failed'

Problem
: The GEGL build breaks with a message like this:

```sh
Workspace/gegl/gegl/gegl-introspection-support.h:162:
Warning: Gegl: GObject-Introspection specific GTK-Doc tag "Rename to" has been deprecated,
please use annotations on the identifier instead:
 * Rename to: gegl_buffer_set
   ^
/home/username/Build/gegl-master-linux/gegl/tmp-introspect4OKYKn/Gegl-0.3.o:
(.data+0x4): undefined reference to `gegl_orientation_get_type'
collect2: error: ld returned 1 exit status
linking of temporary binary failed: Command '['/bin/bash', '../libtool',
'--mode=link', '--tag=CC', '--silent', 'cc', '-o',
'/home/username/Build/gegl-master-linux/gegl/tmp-introspect4OKYKn/Gegl-0.3',
'-export-dynamic', '-I/home/username/Builds/gimp-master-linux/include',
'-I/usr/include', '-g', '-O3', '-mmmx', '-msse', '-ftree-vectorize',
'-ffast-math', '-Wall', '-Wdeclaration-after-statement', '-Wmissing-prototypes',
'-Wmissing-declarations', '-Winit-self', '-Wpointer-arith', '-Wold-style-definition',
'-DG_LOG_DOMAIN="GEGL-"__FILE__', '-L/usr/lib', '-L/usr/lib/i386-linux-gnu',
'-L/home/username/Builds/gimp-master-linux/lib',
'/home/username/Build/gegl-master-linux/gegl/tmp-introspect4OKYKn/Gegl-0.3.o',
'-L.', 'libgegl-0.3.la', '-lgio-2.0', '-lgobject-2.0', '-Wl,--export-dynamic',
'-lgmodule-2.0', '-pthread', '-lglib-2.0']'
returned non-zero exit status 1
/usr/share/gobject-introspection-1.0/Makefile.introspection:153: recipe for target 'Gegl-0.3.gir' failed
make[3]: *** [Gegl-0.3.gir] Error 1
```

Cause
: You triggered regeneration of autogenerated files, which overwrite new ones from the repository.

Solution
: * Clean your build directory:
      + If you have built in the GEGL source directory, run `git clean -xdf`
      + If you have built in another directory than your source directory: `cd $build_directory && rm -rf *`
  * Build GEGL again.

### GIMP

#### GIMP build breaks for missing files in `$ANOTHER_PREFIX/lib`

Problem
: Building GIMP fails for instance in libgimpcolor with a message telling you, that some files in `$ANOTHER_PREFIX/lib` are missing and thus building `libgimpcolor.la` (or another `*.la` file) failed.

Cause
: You switched over to another prefix. You already cleaned or removed that old prefix, but in your new prefix are some files referring to other files in that old prefix.
In other words - there's an evil mixture of your new prefix and some old prefix zombies and libtool trips you up ;-)

Solution
: Finding out the actually errorneous parts can take you hours, so it's better to make a clean sweep:

* Check your environment variables, especially the build environment variables. If they point to that old prefix, change them to point to the new prefix.
* Drop the contents of your new prefix.
* Rebuild from scratch. For babl, GEGL and GIMP (and other libraries, if you use them):
    + Change to their directory.
    + Save your work (backup your working files or do `git stash`).
    + Run `git clean -xdf` or drop the contents of the workspace except the `.git` folder or the source tarball.
    + Run `./configure --prefix=$PREFIX` (git users: `./autogen.sh --prefix=$PREFIX`).
    + Run `make && make install`.
    + Change to their directory.
    + Save your work (backup your working files or do `git stash`).
    + Run `git clean -xdf` or drop the contents of the workspace except the .git folder or the source tarball.
    + Run `./configure --prefix=$PREFIX` (Git users: `./autogen.sh --prefix=$PREFIX`).
    + Run `make && make install`.
* Change to their directory.
* Save your work (backup your working files or do `git stash`).
* Run `git clean -xdf` or drop the contents of the workspace except the .git folder or the source tarball.
* Run `./configure --prefix=$PREFIX` (git users: `./autogen.sh --prefix=$PREFIX`).
* Run `make && make install`.

Some more information on building can be found at [[Hacking:Building| the building tutorials]].

#### Building development GIMP 2.99 breaks for missing dependency gegl-0.4 &gt; 0.4.xx

Problem
: Building the development version of GIMP (2.99) fails with a message that the dependency gegl-0.4 &gt; 0.4.xx is not met.

Cause
: To build the development version of GIMP (2.99) often requires the
development version (bleeding edge) GEGL.  In other words, the GIMP
unstable version can depend on the GEGL unstable version.

Cloning the GEGL repository will get the development version of GEGL.
(But the latest commit may not be tagged yet in the repository, so you
can't ask git for the tagged commit. The `meson.build` file in the
repository will usually show the *next* version that gegl.org will soon
tag and release as stable)

Tagged versions of GEGL are captured by gimp.org in a tarball at
[https://download.gimp.org/pub/gegl/0.4/](https://download.gimp.org/pub/gegl/0.4/),
but not the development version that a development build of GIMP
may require.

Solution
: After you have downloaded, built, and installed GEGL, to check the
installed version, run: `pkg-config gegl-0.4 --modversion`

#### Configure fails with a syntax error near unexpected token `x11,'

Problem
: configure fails with a message about a syntax error but there's no obvious syntax error.

Example:

```sh
./configure: line 22865: syntax error near unexpected token `x11,'
./configure: line 22865: `GTK_CHECK_BACKEND(x11, gtk_required_version,'
```

Cause
: You're missing `/usr/share/aclocal/gtk-3.0.m4`, which is part of the GTK3 development package.

Solution
: Make sure you have `libgtk-3-0` installed, including any associated `-dev` or `-devel` package.

#### GIMP build fails for missing babl function

Problem
: GIMP build fails for missing babl function

Example:

```sh
  /home/Workspace/gimp/app/core/libappcore.a(gimphistogram.o): In function `gimp_histogram_calculate':
  /home/Workspace/gimp/app/core/gimphistogram.c:261: undefined reference to `babl_format_get_model'
   operations/libappoperations.a(gimpoperationmaskcomponents.o): In function `gimp_operation_mask_components_prepare':
  /home/Workspace/gimp/app/operations/gimpoperationmaskcomponents.c:150: undefined reference to `babl_format_get_model'
  gegl/libappgegl.a(gimp-babl.o): In function `gimp_babl_format_get_base_type':
  /home/Workspace/gimp/app/gegl/gimp-babl.c:361: undefined reference to `babl_format_get_model'
  gegl/libappgegl.a(gimp-babl.o): In function `gimp_babl_format_get_linear':
  /home/Workspace/gimp/app/gegl/gimp-babl.c:454: undefined reference to `babl_format_get_model'
  gegl/libappgegl.a(gimp-babl-compat.o): In function `gimp_babl_format_get_image_type':
  /home/Workspace/gimp/app/gegl/gimp-babl-compat.c:38: undefined reference to `babl_format_get_model'
  collect2: error: ld returned 1 exit status
```

Cause
: You have an older version of babl installed on your system, either in
your local installation prefix or by a package. In this case the package
`libbabl-dev 0.1.10-1` was installed. Although the build environment
variables were set to rely on the local installation prefix, the linker
checked this old version and failed for not finding the symbol
`babl_format_get_model` in BABL.

Solution
: * Check the file `$prefix/lib/pkgconfig/babl.pc`. If you find a line
    with `Version: 0.1.10` or older, then your babl library is too old.
    In this case, goto step 2, otherwise to step 4.
  * Change into your babl source directory. Fetch the latest babl source
    code, i.e. from git (branch `master`).
  * Clean and rebuild babl from scratch: run `autogen.sh` or
    `configure`, then `make clean && make uninstall && make && make
    install`
  * In your package manager check the version of the babl development
    library (on Debian systems: `libbabl-dev`, on Mandriva: `libbabl-devel`,
    on Red Hat systems: `babl*src`, on openSuSE systems: `babl-devel`).
    If you find version 0.1.10 or older, then your babl library is too
    old. In your package manager remove it completely and also the GEGL
    development package depending on it.
  * Also clean and rebuild GEGL from scratch to avoid version mismatch
    between GEGL and GIMP.
  * Clean and rebuild GIMP from scratch.

#### Build of GIMP master fails in gimpcursor.c

Problem
: Build of GIMP master fails in gimpcursor.c

Error messages:

```sh
/home/Workspace/gimp/app/widgets/gimpcursor.c:90:5: error: ‘cursor_corner_top’ undeclared here (not in a function)
/home/Workspace/gimp/app/widgets/gimpcursor.c:98:5: error: ‘cursor_corner_right’ undeclared here (not in a function)
/home/Workspace/gimp/app/widgets/gimpcursor.c:106:5: error: ‘cursor_corner_bottom’ undeclared here (not in a function)
/home/Workspace/gimp/app/widgets/gimpcursor.c:114:5: error: ‘cursor_corner_left’ undeclared here (not in a function)
/home/Workspace/gimp/app/widgets/gimpcursor.c:126:5: error: ‘cursor_side_top_right’ undeclared here (not in a function)
/home/Workspace/gimp/app/widgets/gimpcursor.c:134:5: error: ‘cursor_side_bottom_right’ undeclared here (not in a function)
/home/Workspace/gimp/app/widgets/gimpcursor.c:142:5: error: ‘cursor_side_bottom_left’ undeclared here (not in a function)
/home/Workspace/gimp/app/widgets/gimpcursor.c:150:5: error: ‘cursor_side_top_left’ undeclared here (not in a function)
```

Cause
: It's probably a gdk-pixbuf issue. It is for instance known to update the system loaders cache instead the cache of its local installation, if it's installed in a prefix which is in the PATH variable.

Workaround A (the short tour)
: * `cd cursors`
  * `make clean`
  * `cd ..`
  * `make`


Workaround B (the big tour)
: * `gdk-pixbuf-query-loaders --update-cache` (in case this doesnt work run it with sudo)
  * add the pkg-config path of `libgdk-pixbuf2.0-dev` to the `PKG_CONFIG_PATH` environment variable
  * `configure/autogen` with your prefix
  * `make clean`
  * `make`

Anyway, these are just workarounds. If you know the cause and a proper solution, feel free to edit this article.

#### Errors on linking a static library, for instance with MinGW

Problem
: Errors on linking a static library, for instance with MinGW

Compiling GIMP fails, because of not finding a library which is actually there (for instance gexiv2).

The output of <code>make V=1</code> produces a message like this:

```sh
*** Warning: This system can not link to static lib archive
/$your_workspace/share/crossroad/roads/w64/lib/libgexiv2.la.
*** I have the capability to make that library automatically link in when
*** you link to this library.  But I can only do this if you have a
*** shared version of the library, which you do not appear to have.
```

Cause
: You have not a shared (=dynamic) binary version, but a static binary of that particular library. Linking static binaries of most libraries is not supported by GIMP.

You can recognize this by the file extensions:

* Dynamic libraries have the extensions: `so` on Linux, `dylib` on OS X, `dll` on Windows.
* Static libraries have the extensions: `a` on Linux and OS X, `lib` on Windows.

Solution
: 1. Get a dynamic binary version of that library. Either the package manager of your distribution or the web holds one or you
have to compile it yourself.
  2. Compile GIMP again (starting with <code>configure</code> or <code>autogen.sh</code> to let the Autotools find the new library and make it included.)

#### GIMP build fails with message 'Couldn't recognize the image file format for file ./cursor-bad.png'

Problem
: Building GIMP fails with message 'Couldn't recognize the image file format for file ./cursor-bad.png' 

The whole message is

```sh
Making all in cursors
make[2]: Entering directory `$your_workspace/gimp/cursors'
  GEN    tool-cursors.list
  GEN    gimp-tool-cursors.h

failed to load "./cursor-bad.png": Couldn't recognize the image file format for file './cursor-bad.png'
```

Cause
: The environment variable `XDG_DATA_DIRS` is modified in your build environment, which confuses the GLib library (GDK-Pixbuf).

Solution
: * Don't modify the environment variable `XDG_DATA_DIRS` in your build
    environment. I.e. if you build from a build script, don't set this
    variable there.
  * `make distclean`
  * `autogen.sh` or `configure`
  * `make`
  * `make install`

#### GIMP master refuses to start with error 'GIMP requires the GEGL operation "gegl:alien-map"'

Problem
: GIMP master refuses to start and instead shows this error message (or similar, the name might be other than "alien-map")

```sh
GEGL operation missing!

GIMP requires the GEGL operation "gegl:alien-map".
This operation cannot be found. Check your
GEGL install and ensure it has been compiled
with any dependencies required for GIMP.
```

Cause
: You have GEGL 0.2 installed on your system, but GIMP master requires GEGL master. Or you have GEGL master installed in your GIMP prefix, but GIMP fails to find it.
Master means the latest version of the branch named "master" in the GEGL repository.  In other words, the GIMP project "tracks" the GEGL project.

The GEGL version changes.  As of this writing, GIMP 2.99 uses gegl-0.4, but that may change in the future.

Solution
: To tell GIMP master where GEGL master is run

```sh
export GEGL_PATH=${INSTALL_PREFIX}/lib/gegl-0.3
```

… before starting GIMP.

To avoid a similar version mismatch with Babl, you can also run:

```sh
export BABL_PATH=${INSTALL_PREFIX}/lib/babl-0.1
```

… before starting GIMP.

Details: at startup, GIMP checks the GEGL library for the presence of a certain set of GEGL operations.
The operations are present as string literals in the binary.  You can test whether your GEGL library contains an operation using e.g.:

```sh
grep -R "gegl:alien-map" /usr/local/lib/gegl-04
```

#### Unit tests fail

Problem
: Many unit tests fail with an output similar to this:

```sh
FAIL: test-save-and-export
#### ==================

Xlib:  extension "RANDR" missing on display ":99".
GIMP-Error: Could not open '/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_build/../app/tests/gimpdir/themerc' for writing: Permission denied

GIMP-Error: Failed to open file '/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_inst/etc/gimp/2.0/templaterc': open() failed: No such file or directory


(/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_build/app/tests/.libs/lt-test-save-and-export:15852): Gimp-Widgets-WARNING **: GDK returned bogus values for the monitor resolution, using 96 dpi instead.

FAIL: test-session-2-6-compatibility
#### ============================

Xlib:  extension "RANDR" missing on display ":99".
/gimp-session-2-6-compatibility/read_and_write_session_files: GIMP-Error: Could not open '/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_build/../app/tests/gimpdir/themerc' for writing: Permission denied

GIMP-Error: Failed to open file '/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_inst/etc/gimp/2.0/templaterc': open() failed: No such file or directory


(/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_build/app/tests/.libs/lt-test-session-2-6-compatibility:16107): Gimp-Widgets-WARNING **: GDK returned bogus values for the monitor resolution, using 96 dpi instead.

FAIL: test-session-2-8-compatibility-multi-window
#### =========================================

Xlib:  extension "RANDR" missing on display ":99".
/gimp-session-2-8-compatibility-multi-window/read_and_write_session_files: GIMP-Error: Could not open '/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_build/../app/tests/gimpdir/themerc' for writing: Permission denied

GIMP-Error: Failed to open file '/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_inst/etc/gimp/2.0/templaterc': open() failed: No such file or directory


(/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_build/app/tests/.libs/lt-test-session-2-8-compatibility-multi-window:16351): Gimp-Widgets-WARNING **: GDK returned bogus values for the monitor resolution, using 96 dpi instead.

FAIL: test-session-2-8-compatibility-single-window
#### ==========================================

Xlib:  extension "RANDR" missing on display ":99".
/gimp-session-2-8-compatibility-single-window/read_and_write_session_files: GIMP-Error: Could not open '/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_build/../app/tests/gimpdir/themerc' for writing: Permission denied

GIMP-Error: Failed to open file '/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_inst/etc/gimp/2.0/templaterc': open() failed: No such file or directory


(/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_build/app/tests/.libs/lt-test-session-2-8-compatibility-single-window:16595): Gimp-Widgets-WARNING **: GDK returned bogus values for the monitor resolution, using 96 dpi instead.

FAIL: test-single-window-mode
#### =====================

Xlib:  extension "RANDR" missing on display ":99".
GIMP-Error: Could not open '/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_build/../app/tests/gimpdir/themerc' for writing: Permission denied

GIMP-Error: Failed to open file '/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_inst/etc/gimp/2.0/templaterc': open() failed: No such file or directory


(/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_build/app/tests/.libs/lt-test-single-window-mode:16839): Gimp-Widgets-WARNING **: GDK returned bogus values for the monitor resolution, using 96 dpi instead.

FAIL: test-tools
#### ========

Xlib:  extension "RANDR" missing on display ":99".
GIMP-Error: Could not open '/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_build/../app/tests/gimpdir/themerc' for writing: Permission denied

GIMP-Error: Failed to open file '/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_inst/etc/gimp/2.0/templaterc': open() failed: No such file or directory


(/var/lib/jenkins/workspace/gimp-master/gimp-2.9.1/_build/app/tests/.libs/lt-test-tools:17083): Gimp-Widgets-WARNING **: GDK returned bogus values for the monitor resolution, using 96 dpi instead
```

Cause
: Look at the last error line for each failing test and start your research there.

Solution
: In case of `Gimp-Widgets-WARNING **: GDK returned bogus values for the monitor resolution, using 96 dpi instead`:

The function `g_warning` was used for an error other than a programming error. Use the `g_printerr` function instead. See also [commit a07f230 on GIMP master](https://git.gnome.org/browse/gimp/commit/?id=a07f2301bbdf44ef00fba738f2ba157cd3546e39)

#### Meson build yields error message: 'meson.build:1:0: ERROR: Value never is not boolean (true or false).'

Problem
: As above, or 'Value always is not boolean'

Cause
: You have used 'never' or 'always' in the build options of the meson command e.g. `-Djavascript=never`

Solution
: Use 'true' or 'false'. Since about mid 2020.

#### Meson build hangs i.e. never finishes

Problem
: In the console where you started the meson build, near the end of the build process, meson stops emitting messages and the command line prompt never comes back.

Cause
: Some step in the build did not complete.
The default for meson is `-j 3`, meaning build in parallel, so the last step shown in the console might not be the step that hung.

Solution
: Use `ps -ef` to determine which process is hung, and debug that build step.
Sometimes it is the `g-ir-scanner` tool.
Or use `-j 1` so that the last step shown is the step that hung.

#### build configure step stops: missing gtk-encode-symbolic-svg

Problem
: The executable tool `gtk-encode-symbolic-svg` could not be found.

Cause
: You have not installed the package containing the tool.

Solution
: Use tools provided by your distribution to find which package contains
`gtk-encode-symbolic-svg`, and install that package.  Since Ubuntu 20.04
focal, the tool `gtk-encode-symbolic-svg` is in Ubuntu package
`libgtk-3-bin`.
Formerly in Ubuntu package `gtk-3-examples`.

### JHBuild

#### JHBuild ignores autogenargs

Problem
: JHBuild ignores the `autogenargs` parameter for a target.

Cause
: A simple typo. In this case it ended with duplicated quotes and was after the closing bracket of the autotools tag.

Solution
: * Use the proper syntax: `<autotools ....="" autogenargs="..."> </autotools>`
  * If the options in the JHBuild menu on the command line don't help, cancel the JHBuild process (option '3'-give up on module') and run it again.

### See also

* [Autotools questions at Stackoverflow.com](http://stackoverflow.com/questions/tagged/autotools?sort=frequent&amp;pageSize=15)

* [Clang users mailing list](http://lists.cs.uiuc.edu/mailman/listinfo/cfe-users)
* [Clang and LLVM questions at Stackoverflow.com](http://stackoverflow.com/questions/tagged/clang+or+llvm)

* [The GCC-Help mailing list](http://gcc.gnu.org/ml/gcc-help/)
* [GCC questions at Stackoverflow.com](http://stackoverflow.com/questions/tagged/gcc)

* [GEGL Developer mailing list](https://mail.gnome.org/archives/gegl-developer-list/)
* <a class="external text" href="irc://irc.gimp.org/#gegl" rel="nofollow">GEGL IRC channel</a>

* [GIMP Developer mailing list](https://mail.gnome.org/archives/gimp-developer-list/)
* <a class="external text" href="irc://irc.gimp.org/#gimp" rel="nofollow">GIMP IRC channel</a>

* [Jenkins mailing lists](http://jenkins-ci.org/content/mailing-lists)

## Code

### Eclipse

#### Menu items 'Reconfigure' and 'Autotools/...' are disabled

Problem
: When right-clicking on a CDT project, the item Reconfigure and the items in the Autotools submenu are disabled 
Problem
: Reconfiguring a project ends with an error 'The chosen operation is not enabled' 

Neither cleaning or make distclean bring them back.

Cause
: Your project configuration is perhaps garbled.

Solution A
: In a terminal window change to the project's workspace folder, enter
`make distclean` and `./autogen.sh` resp. `./configure`. Every time you
want to reconfigure your project repeat this step. Subsequently building
the project in Eclipse works despite the disabled Reconfigure/Autotools
menu items.

Solution B
: * Delete the project (but **don't tick** "*Delete project contents on disk (cannot be undone)*"!!)
  * Create the project again: Makefile project from existing sources, then New/Convert to Autotools project
  * In the project's properties configure the Autotools, Paths and Symbols (Include Path, Referenced projects)

#### Reconfigure fails with message 'aclocal: error: non-option arguments are not accepted: '-I'

Problem
: Reconfiguring a CDT Autotools project causes error 'aclocal: error: non-option arguments are not accepted: '-I' 

Cause
: In the global or project specific environments settings the ACLOCAL_FLAGS variable is embraced with quotes and Eclipse somehow can't handle this.

Solution
: * Edit the project's environments settings (Properties/ C/C++ Build / Environment). If the ACLOCAL_FLAGS variable doesn't appear here or is not printed boldly, then cancel that dialog and edit them in Window / Preferences / C/C++ Build / Environment).
  * Remove the quotes from the ACLOCAL_FLAGS variable.

#### Basic tools not found although there

Problem
: Trying to build, clean or reconfigure the project ends with an error, that either sh, make, clang or clang++ could not be found although they are there.

Cause
: Eclipse doesn't find them in the `PATH` variable. Either you didn't include `${PATH}` in your `PATH` settings or you included it, but Eclipse doesn't resolve it.

Solution
: * Edit the project's environments settings (Properties/ C/C++ Build / Environment). If the `PATH` variable doesn't appear here or is not printed boldly, then cancel that dialog and edit them in Window / Preferences / C/C++ Build / Environment).
  * Make sure your `PATH` variable setting contains the string `:${PATH}`. It must have the curly braces around it! If the variable is already there and isn't resolved anyway, then get its value from your operating system and replace `${PATH}` with that value.

#### Eclipse doesn't recognize CDT/Autotools project anymore

Problem
: After starting Eclipse has closed my CDT project. When trying to reopen it I get the message "*The project description file (.project) for '${My project}' is missing.  This file contains important information about the project.  The project will not function properly until this file is restored.*"

Problem
: Eclipse tells in the project's properties that the project is not a CDT project (anymore).

Cause
: Eclipse's projects settings got lost. One candidate is having run `git clean -xdf` in the workspace.

Solution
: If you have a backup of the files .project, .cproject, .autotools, then restore them now to the project's folder. If you don't have them at hand:

* Create a new dummy C project and convert it to an Autotools C project (New/CDT/Convert to Autotools project).
* Copy the files .project, .cproject, .autotools from that project's folder into the folder of your damaged project.
* Edit the projects CDT or Autotools settings.
* Create a backup of the files .project, .cproject, .autotools.

### See also

* [Eclipse CDT support site](http://eclipse.org/cdt/support.php)
* [Eclipse CDT questions at Stackoverflow.com](http://stackoverflow.com/questions/tagged/cdt)

* [Emacs support](https://www.gnu.org/software/emacs/#HelpMailing)
* [Emacs questions at Stackoverflow.com](http://stackoverflow.com/questions/tagged/emacs)

* [Geany FAQ](http://www.geany.org/Documentation/FAQ)
* [Geany questions at Stackoverflow.com](http://stackoverflow.com/questions/tagged/geany)

* [GEdit support](https://wiki.gnome.org/Apps/Gedit)
* [GEdit questions at Stackoverflow.com](http://stackoverflow.com/questions/tagged/gedit)

* [GEGL Developer mailing list](https://mail.gnome.org/archives/gegl-developer-list/)
* <a class="external text" href="irc://irc.gimp.org/#gegl" rel="nofollow">GEGL IRC channel</a>

* [GIMP Developer mailing list](https://mail.gnome.org/archives/gimp-developer-list/)
* <a class="external text" href="irc://irc.gimp.org/#gimp" rel="nofollow">GIMP IRC channel</a>

* [Netbeans mailing lists and forums](https://netbeans.org/community/lists/index.html)
* [Netbeans questions at Stackoverflow.com](http://stackoverflow.com/questions/tagged/netbeans)

* [Basic vi commands](http://www.cs.colostate.edu/helpdocs/vi.html)
* [Vi questions at Stackoverflow.com](http://stackoverflow.com/questions/tagged/vi)

## Debug

### See also

* <a class="external text" href="irc://irc.freenode.net:6667/#gdb" rel="nofollow">GDB IRC channel</a>
* [GDB wiki](http://sourceware.org/gdb/wiki/)
* [GDB questions at Stackoverflow.com](http://stackoverflow.com/questions/tagged/gdb)

* [GEGL Developer mailing list](https://mail.gnome.org/archives/gegl-developer-list/)
* <a class="external text" href="irc://irc.gimp.org/#gegl" rel="nofollow">GEGL IRC channel</a>

* [GIMP Developer mailing list](https://mail.gnome.org/archives/gimp-developer-list/)
* <a class="external text" href="irc://irc.gimp.org/#gimp" rel="nofollow">GIMP IRC channel</a>

* [LLDB questions at Stackoverflow.com](http://stackoverflow.com/questions/tagged/lldb)

## Manage versions

### git

#### I'm a few commits behind

Problem
: After checking out a particular branch, git tells me, that I'm a few
commits behind, for example: '*Your branch is behind 'origin/gimp-2-10'
by 2 commits, and can be fast-forwarded.*'

Cause
: On `git pull`, git didn't update the HEAD on your local branch for whatever reason.

Solution
: Try `git pull origin` to update your local branch.

#### git marks untouched files as dirty

Problem
: `git status` shows that there are untracked files I haven't touched at all

For instance this:

```sh
$:~/Workspace/gimp$ git status
On Branch master

Untracked files:

	app/base/
	app/composite/
	app/paint-funcs/
	plug-ins/file-xjt/
	plug-ins/metadata/
```

These files are not in the current branch, but in another (for instance `gimp-2-10`).

Cause
: For some reasons a recent `git checkout` did not delete them. Now these zombies lie around to frighten poor developers.

Solution
: * Remove them from your disk.
  * If you noticed this error in your IDE then refresh your workspace there (in Eclipse: right click on your project, then click "Refresh").

#### Pull error: 'Insufficient permission for adding an object to repository database .git/objects'

Problem
: On pulling git shows me the aforementioned error.

Cause
: Permission issues in your file system.

Solution
: * Change into your directory where the project's `.git/` folder resides. Usually this is the working directory.
  * On Linux or OS X run `sudo chown -R gituser:gituser .git/objects` (replace gituser with your user name).
    On Windows right click on the `.git/objects` folder, choose 'Properties', choose the 'Security' tab, click the 'Advanced' button, choose the 'Owner' tab, click the 'Edit...' button. Set the file owner for that folder and all files and subdirectories in it. See also the [Techotopia tutorial](http://www.techotopia.com/index.php/Understanding_Windows_Server_2008_File_and_Folder_Ownership_and_Permissions#Taking_and_Transferring_Windows_Server_2008_File_and_Folder_Ownership)

### Eclipse

#### Eclipse marks untouched files as dirty

Problem
: Eclipse shows dirty files and folders I have never touched.

[See the related Git error](#git-marks-untouched-files-as-dirty)

### See also

* [Git documentation (man pages, reference manual, videos, links)](https://git-scm.com/doc)
* [The Git online book](https://git-scm.com/book)
* [Git community support](https://git-scm.com/community)
* [Git questions at Stackoverflow.com](https://stackoverflow.com/questions/tagged/git)

* [GIMP Developer mailing list](https://mail.gnome.org/archives/gimp-developer-list/)
* <a class="external text" href="irc://irc.gimp.org/#gimp" rel="nofollow">GIMP IRC channel</a>
* [GEGL Developer mailing list](https://mail.gnome.org/archives/gegl-developer-list/)
* <a class="external text" href="irc://irc.gimp.org/#gegl" rel="nofollow">GEGL IRC channel</a>

* [Eclipse EGit support site](https://eclipse.org/egit/support/)
* [Eclipse JGit support site](https://www.eclipse.org/jgit/support/)
* [Eclipse EGit questions at Stackoverflow.com](https://stackoverflow.com/questions/tagged/egit)

## Translate

### Lots of merge conflicts in GIMP master

Problem
: I'm translating on the current production branch of GIMP (`gimp-2-10`, etc.) and on `master`. I'd like to cherry-pick my changes from the production branch to the master branch. But I get lots of merge conflicts and changes I never introduced.

Cause
: Cherry-picking translations from the production branch into the master branch doesn't work.

Solution
: Commit your translation updates directly into the master branch or try to merge your translation branch into the master branch.

### See also

* [GEGL Developer mailing list](https://mail.gnome.org/archives/gegl-developer-list/)
* <a class="external text" href="irc://irc.gimp.org/#gegl" rel="nofollow">GEGL IRC channel</a>

* [GIMP Developer mailing list](https://mail.gnome.org/archives/gimp-developer-list/)
* <a class="external text" href="irc://irc.gimp.org/#gimp" rel="nofollow">GIMP IRC channel</a>

* [GNU gettext manual](http://www.gnu.org/software/gettext/manual/gettext.html)
* [GNU gettext FAQ](http://www.gnu.org/software/gettext/FAQ.html)

* [GNOME Translation project wiki](https://wiki.gnome.org/TranslationProject)

* [Poedit support](http://www.poedit.net/support.php)
* [Poedit questions at Stackoverflow.com](http://stackoverflow.com/questions/tagged/poedit)

* [Virtaal documentation](http://docs.translatehouse.org/projects/virtaal/en/latest/)

## Runtime Errors

Warnings and errors displayed in a terminal/console in which you started GIMP from a command line.

Many are just clutter, that make it hard to find severe errors from code you are testing.

### 'GEGL-Message: 14:09:17.687: Module '/usr/local/lib/x86_64-linux-gnu/libgegl-npd-0.4.so' load error: Missing gegl_module_query() symbol ?'

Problem
: GIMP starts, but the terminal displays many messages similar to the above, when plugins are loaded.
It is not clear whether these messages affect functioning, they might just be annoying warnings.

Cause
: Missing environment variables.

Solution
: See above under 'Building'.  The environment variable `GEGL_PATH` must
reference the top GEGL directory, not just its parent. Referencing the
parent is sufficient for GIMP to start, but not for plugins to load
without these warnings.

### GIMP fails to start with error, 'GIMP requires the GEGL operation "gegl:alien-map"'

Problem
: GIMP starts fails to start, with the message above.

Cause
: Missing environment variables.

Solution
: See above under 'Building'

### A plugin fails to appear in the menus, with error like "GIMP-WARNING: gimp-2.xx: gimp_wire_read(): error"

Problem
: GIMP starts, but a plugin does not appear in the GIMP menus as expected, and the terminal shows the above message.

Cause
: At startup, GIMP searches certain directories for plugins (executables and interpretable text files) and "queries" them.
This means it starts them in a separate process and then communicates with them over a 'wire' i.e. interprocess communication.
If the plugin crashes or fails to respond, Gimp gives the error above.
You probably installed a plugin that is fatally flawed.

Solution
: Remove the plugin.  To see which plugin to remove, start GIMP with the `--verbose` flag, and it will print the pathname of plugin files as it queries them.

To debug the flawed plugin, you can use print statements.

An interpreted plugin may fail very early in its text, for example in the hashbang i.e. `#!/usr/bin/env python3` statement on the first line,
or in an import statement.

### Chain of python exceptions beginning with gi.RepositoryError: Typelib file for namespace 'Gegl', version '0.4' not found

Problem
: On startup, GIMP starts but shows a long chain of Python exceptions.  The root exception (the first in the list) is as above.  Affects loading of plugins.

Cause
: Python is using GOBject introspection, but can't find the typelibs for GIMP. Typelibs are in a directory named `girepository-1.0`. The GIMP typelib is named `Gimp-3.0.typelib`.

Solution
: Define environment variable `GI_TYPELIB_PATH`.  For example, `export
GI_TYPELIB_PATH=/usr/local/lib/x86_64-linux-gnu/girepository-1.0:/usr/local/lib/girepository-1.0`.
The example is for Ubuntu 19.10.  You may want to change the prefix of
the paths, for example using the `GIMP_PREFIX` environment variable (if
you use it.)

The typelibs for GIMP, GEGL, and babl might be on different paths (as in the example).

### Gtk-Message: 19:04:46.797: Failed to load module "canberra-gtk-module"

Problem
: Terminal often displays message like above.  Does not affect operation, except sounds may be missing?

Cause
: Package `libcanberra-gtk3-module` is missing.

Solution
: Install missing package, for example: `sudo apt-get install libcanberra-gtk3-module`

Note that you may still see the error if you are using a self-built
GTK+3. In this case, the `libcanberra` module installed by the package
manager is not visible by the GTK installed in a prefix.

You may redirect the self-built GTK to look into the system prefix for
modules through the
[`GTK_PATH`](https://docs.gtk.org/gtk3/running.html#environment-variables)
environment variable. For instance:

```sh
export GTK_PATH=/usr/lib/x86_64-linux-gnu/gtk-3.0/
```

If ever you experience incompatibilities (since the system GTK and the
self-built one may have been built differently), you may also decide to
ignore the message, as it's harmless (unlike you needed to use this
module, but chances are low this would ever be needed for GIMP).

### (gimp-2.99:xx): dbind-WARNING **: 09:35:43.023: Couldn't register with accessibility bus:

Problem
: Terminal often shows message like above.  Does not affect operation, unless you need accessibility i.e. are visually or hearing impaired.

Cause
: Seems to be a long-standing bug in Linux distributions.

Solution
: In your environment: `export NO_AT_BRIDGE=1`

### luajit: ...mp/2.99/plug-ins/goat-exercise-lua/goat-exercise-lua.lua:22: module 'lgi' not found:

Problem
: On startup, terminal shows a message that begins like above and ends with "GIMP-WARNING: gimp-2.99: gimp_wire_read(): error".
Affects loading of lua language plugins.
In the development branch of GIMP, a set of "goat-exercise-foo" plugins exercise some GObject Introspection language bindings (C, Python, Lua, JS, Scheme, Perl ?)

Cause
: Package `lua-lgi` is missing.

Solution
: Install the package.

### On Ubuntu:  Gtk:ERROR:../../../../gtk/gtkiconhelper.c:494:ensure_surface_for_gicon: assertion failed (error == NULL): Icon 'image-missing' not present in theme Symbolic (gtk-icon-theme-error-quark, 0====

Problem
: On Ubuntu (only?), when using a plugin that opens a Gtk3 file chooser widget, terminal shows a message as above, and plugin crashes.

Cause
: Package `gnome-icon-theme` is missing.  See [#2029](https://gitlab.gnome.org/GNOME/gimp/-/issues/2029).

Solution
: Install the package. If you are not building GIMP yourself, apparently this issue is fixed by the packaging of GIMP having a dependency on package `gnome-icon-theme`.

### (gimp-2.99:xx): dconf-WARNING **: 13:59:42.430: failed to commit changes to dconf: Failed to execute child process ?dbus-launch? (No such file or directory)

Problem
: The message appears as you start a plugin. It seems harmless unless you have configured GLib to stop on warnings, or if you want to use the services of [dbus](https://en.wikipedia.org/wiki/D-Bus)

Cause
: This is a warning from GLib. GIMP is usually built to support dbus (but can be configured without that support?). The message means that when GIMP attempted to execute the command `dbus-launch`, it could not be found on the system.

Solution
: Install package `dbus-x11`, or whichever package contains the executable command dbus-launch.
