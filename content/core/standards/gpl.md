---
title: "GIMP Palette Format Version 2 (.gpl)"
Author: "GIMP team"
Date: 2023-12-12
toc: true
---

GIMP Palette files are purely textual, with line-feed (`0x0A`) separated
lines.

* Mime Type: `application/x-gimp-palette`
* Common extension: `.gpl`

## Header

1. The first line of the file **MUST** be `GIMP Palette` (*magic*
   number).
2. If the second line starts with the prefix "`Name: `", the rest of the
   line **MUST** be in UTF-8. This will be the palette name (start and
   end spaces will be stripped).
3. *Optional*: if the next line starts with the prefix "`Columns: `", it
   must be followed by an integer between 0 and 255. This number will
   specify the number of columns to display the palette on. A value of 0
   means that the number of columns will be variable, hence the display
   will be flowing with the interface size.

Alternatively, if the second line doesn't starts with "`Name: `", this
file will be considered in the old format and the palette contents will
start from this second line. In this old format variant, the file
basename (after UTF-8 conversion) will be used as palette name and there
will be no "Columns" value (i.e. that 0 is assumed).

We number the new version 2, even though the format was never formally
versionned until 2023 (as far as we know).

## Body

Each successive line after the header will be either:

* a comment if the line starts with `#` (the rest of the line will be
  ignored);
* a totally empty line (not even spaces) which will be ignored;
* a color description.

Color description lines have the following format:

```txt
r g b any color name you want
```

`r`, `g`, and `b` are integers and their values must be between 0 and
255.

Any string after the 3 integers, up until the end of the line (apart
from start and end spaces which are stripped) will be considered the
color name. The color name is optional, i.e. that a color description
line may only contain `r g b`. The color name string **MUST** be in
UTF-8.

The 3 integers represent respectively the red, green and blue component
in the sRGB color space. As a consequence, up to version 2, GPL has the
following limitations:

* it only supports RGB color model;
* it only supports sRGB color space;
* bit depth is 8 bits per components only;
* alpha component (transparency) is not supported;
* colors are bounded (to sRGB).
