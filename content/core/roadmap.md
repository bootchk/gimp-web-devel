---
title: "Roadmaps"
description: "Feature Prioritization"
date: "2022-09-07"
author: "Jehan"
abbrev: "roadmap"
weight: -1
---

## Feature Prioritization

This page shows roughly how the core development team plans GIMP evolution.
Features absent from these roadmaps don't mean we are not interested: since GIMP
is developed as a community, all it takes to revise priority is for someone to
contribute.

ℹ️ **Below roadmaps are not "holy truth", they are flexible and evolving.**

The bottom line is that GIMP is what we all make of it. There is no
single company or entity heading the software (code admission is based
on quality and usefulness, not dictated by someone's business).

## GIMP 2.10.x (Stable branch roadmap)

The 2.10.x series coexists with the ongoing work to port GIMP to GTK+3
and cleanup obsolete API.

We try to backport fixes and new features to 2.10.x when we can.

This branch is considered pretty stable and our focus really shifted on
GIMP 3.0. It is still possible that new features appear in further 2.10
releases, yet expectation is low.

{{< roadmap "2.10" >}}

[Milestone 2.10 in Gitlab](https://gitlab.gnome.org/GNOME/gimp/-/milestones/2)

## GIMP 3.0 (Development branch roadmap)

The focus of this version is to complete the GTK+3 port and reduce
technical debt in used technologies.

Benefits:

* Better maintained version of GTK
* Better graphics tablet support
* Better handling of HiPPI displays
* Wayland support on Linux
* Refactored code, easier to build new features upon

{{< roadmap "3.0" >}}

[Milestone 3.0 in Gitlab](https://gitlab.gnome.org/GNOME/gimp/-/milestones/3)

## Post-GIMP 3.0.0

After GIMP 3.0.0, we have several projects which we may focus on, in separate
releases. These may happen either on minor (e.g. 3.2.0) or micro releases (e.g.
3.0.2) as our release policy change since 2.10.0 allows us to release new
features on micro versions.

What we ultimately want is to make smaller and faster release points. We
gathered features we want to work on in separate roadmaps, though they may be
spread through several releases, or oppositely released in a single GIMP
version.

Actual release numbers will be refined as we progress.

### Non-destructive layer types

Some work has been started already around other types of layers. This might
even end up in new "shape" abilities (something requested for a long time, which
we wanted to make right, the non-destructive way).

Since some good code advancement already exist for several pieces of this plan,
it may be included in a release soon after GIMP 3.0. Right now we are hoping it
could be part of the first micro point after GIMP 3.0.0, i.e. that it could be
available in **GIMP 3.0.2**.

{{< roadmap "vectors" >}}

### Non-destructive filters

This target was originally dubbed **GIMP 3.2** though we will in fact
release a first implementation in **GIMP 3.0**!

The reason why we had assigned a version number already for years is
that this is a very important step which is a main development focus as
part of the non-destructive editing workflows we are promoting.

Note that both adjustment layers and layer effects/styles are the
terminologies currently used in requests by users.

{{< roadmap "filters" >}}

[Milestone 3.2 in Gitlab](https://gitlab.gnome.org/GNOME/gimp/-/milestones/14)

### Animation and multi-page support

Long-term work has been done around animation concepts. It will also bring
multi-page ability.

{{< roadmap "animation" >}}

### Macros (script recording)

Ultimate continuation of a work which started in GIMP 3.0, where we started to
store parameters of every successful plug-in run. Same should happen for GEGL
operations and tool usage.
Finally we need the proper infrastructure to allow [replaying
actions](https://gitlab.gnome.org/GNOME/gimp/-/issues/8).

{{< roadmap "macro" >}}

### Extensions

The work was started for GIMP 3.0 for which we originally hoped to finalize the
new concept of "GIMP extensions", a new wrapper format (`.gex`) which can embed
plug-ins, brushes, dynamics, themes and other data, (un)installable in 1 or 2
clicks.

The core infrastructure is already present since GIMP 3.0, but we also want to
make this public with a new online infrastructure, allowing third-party
extension creators to upload their extensions and everyone to browse available
ones.

{{< roadmap "extensions" >}}

### Space Invasion

"Space Invasion" is the codename for a long-lived project, started with GIMP
2.10 development, i.e. [back in
2012](https://gimpfoo.de/2012/04/17/goat-invasion-in-gimp/). The concept is to
make GIMP more than a sRGB editor. First we focused on anyRGB, but nowadays we
are even more into anySpace support. We want to be able to edit images with CMYK
backend too (right now, we can do CMYK 🗘 RGB roundtrips which is nice but not
enough), also allowing random channels (e.g. spot color channels), and of course
why not CIELAB images or whatever could be useful.

HDR support is definitely also a good target eventually.

{{< roadmap "space-invasion" >}}

### Tools

We have been planning several new interesting tools or tool enhancements. Some
are still only conceptual, though some already have experimental implementations
which we consider not in a releasable state.

{{< roadmap "tools" >}}

### Canvas

Since GIMP 2.10, we added some advanced canvas concept, such as the ability to
see and even work off-canvas. This is still incomplete, and we still intend to
add new concepts of either auto-growing layers or even infinite layers.

{{< roadmap "canvas" >}}

### GUI

GIMP's interface has always been very customizable, though it can always be
improved. For instance while the "single window mode" ([since
2.8](https://www.gimp.org/release-notes/gimp-2.8.html#single-window-mode)) was
very welcome, in many case, we wished for a mix of the single and multi window
modes. Moreover a horizontal toolbar is often wished for, as well as
improvements of dockable management. These are all topics which could be
redesigned and worked on.

{{< roadmap "gui" >}}

## Miscellenaous

Many of these features are on contributors wishlist and can be implemented in a
branch and merged into the main development branch as we go, especially since we
loosened up the "no new features in stable releases" policy. Please talk to us,
if you want to work on any of those.

{{< roadmap "future" >}}
[Milestone "Future" in Gitlab](https://gitlab.gnome.org/GNOME/gimp/-/milestones/1)
