---
title: "Archive of past internship projects"
description: "All the projects which were developed by interns in the past"
author: "GIMP team"
abbrev: "internship-archives"
weight: 2
---

## Projects in GSoC 2023
### Auto-growing layers

Mentor
: Jehan

Student
: Shubham Daule

Category
: User Interface, Core, Painting

Status
: finished

Integration status
: finished

Project introduction (by the contributor):

> GNU Image Manipulation Program, or GIMP in short, is one of the most popular image editing
programs that is Free and Open Source. But currently, the image layers need to be resized
manually which is inefficient and degrades user experience. This can be particularly frustrating
when using a brush tool. Also, beginners might find it hard to use. So this project aims to add
support for Auto growing layers in GIMP and other features required for implementing it.

Mentor note: the project was implemented and thing went quite smoothly on this
project. Not much to say more than "*good job!*

See also:

* [Merge request !961](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/961)
* [Integration in development version
  2.99.18](https://www.gimp.org/news/2024/02/21/gimp-2-99-18-released/#auto-expanding-layers)

### Improving the Text Tool

Mentors
: Liam Quin, Jehan

Student
: Idriss Fekir

Category
: User Interface, Core, Text

Status
: Scope broken down into smaller tasks

Integration status
: finished

Project introduction (by the contributor):

> GIMP is a high end photo editor, but its text tool is rudimentary. A lot could
be done to improve it UI/UX-wise and feature-wise. I plan on improving
opentype features, mainly support for alternate glyphs, and also some other
improvments.

Mentor (Jehan's) note: the scope was a bit too large at first, but we soon
re-evaluated the project into a more generic improvement on text support. In the
end, even though we couldn't work on more specific OpenType features, this
resulted into an invaluable project which really improved the core
infrastructure for font support in GIMP. The software is better at detecting
wrongly loaded or non-supported fonts (less frustration on "non-working" fonts),
can now load some fonts it used not to be able to load, and in particular is
able to handle different fonts named the same. When needed, we sometimes bypass
some of the dependency libraries when they have known limitations in specific
use cases. In other words, the core code is much more robust. XCF files are also
much better at specifying used fonts in text layers for retrieval on different
machines. A huge success in a different way!

See also:

* [Implement support for fonts with custom
  styles](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/1011)
* [Port `GimpText` to `GimpFont` and fix font (de)serialization in
  XCF](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/1030)
* [Remove code path of not using
  fontconfig](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/1032)
* [ Rename every font in fontconfig, not only the broken
  ones](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/1033)
* [Integration in development version
  2.99.18](https://www.gimp.org/news/2024/02/21/gimp-2-99-18-released/#font-handling-improvements)

### Implement Initial Non-Destructive Editing Workflow in GIMP

Mentor
: Jehan

Student
: Nikc / Alx Sa (NikcDC)

Category
: User Interface, Core, Non-Destructive Editing

Status
: finished

Integration status
: finished

Project introduction (by the contributor):

> GIMP utilizes a Destructive Editing workflow – all changes are made directly to the image itself. A
much requested feature is Non-Destructive Editing (NDE) which would allow users to make changes
without overwriting the original image. This would mean they can easily revert prior edits, modify the
settings of filters to see live changes, and rearrange the order that effects are applied in.
GIMP’s image processing engine GEGL is fully capable of many NDE operations. However, these
features are not fully exposed to users. The goal of this project would be to enable Non-Destructive
Editing on individual layers and layer groups through “Layer Effects” such as drop shadows and
brightness/contrast filters. A basic implementation consists of three milestones:
> 1. Research and design a NDE workflow that compliments GIMP’s current design and user base.
> 2. Implement “Layer Effects” for GIMP’s regular and text layers as well as layer groups.
> 3. Add support to the PSD plug-in to recognize and import equivalent “Layer Styles” and “Adjustment
Layers” as NDE Layer Effects.

Mentor note: promising researches and demos by the contributor, though the GUI
could not be finished. The [UX
specification](/core/specifications/layer-effects-ui/) also came a little later
than expected. Nevertheless many discussions, preparations could happen and we
expect the contributor to continue afterwards (they are known in the project
now, and it's their second GSoC as contributor). Various other code could also
be contributed in the side by this contributor, even when not directly related
to this project.

See also:

* [Initial NDE prototype](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/958)
* [Integration in development version
  2.99.18](https://www.gimp.org/news/2024/02/21/gimp-2-99-18-released/#initial-non-destructive-layer-effects)
* [Specification for future improvements by long-term contributor
  Aryeom](https://developer.gimp.org/core/specifications/layer-effects-ui/)

## Projects in GSoC 2022
### Implement basic CMYK support in GIMP

Mentor
: Jehan

Student
: Nikc / Alx Sa (NikcDC)

Category
: Plug-Ins, User Interface, Core

Status
: finished

Integration status
: finished

One of the most requested features for GIMP is support for CMYK and desktop
publishing - there's even an FAQ entry on their site specifically for it! While
GIMP is primarily focused on digital editing, their backend systems and
libraries are more than capable of CMYK workflows. The goal of this project is
to enable users to begin using GIMP for creating images for print. A basic
implementation consists of three milestones:

1. Allowing users to import and export the most common "print" file formats
   (.tiff, .pdf/.eps/.ai, .psd) with CMYK profiles.
2. Developing a Dockable Dialogue to simplify the process of changing profiles,
   enabling soft-proofing, and other related tools.
3.  Identifying and improving existing issues with Color Management as part of
    GIMP's Color Space Invasion initiative. While this basic implementation
    would not fully implement the "High-end CMYK support" goal in GIMP's
    roadmap, it would be a step in that direction, and could encourage users and
    developers to contribute further to that effort.

See also:

* [GSoC 2022 project announced: CMYK
  features](https://www.gimp.org/news/2022/06/03/cmyk-in-gsoc-2022/)
* [Integration in development version
  2.99.12](https://www.gimp.org/news/2022/08/27/gimp-2-99-12-released/#cmyk)
* [Integration in development version
  2.99.14](https://www.gimp.org/news/2022/11/18/gimp-2-99-14-released/#psd)

## FSF intern in 2018
### Improving JPEG2000 support

Mentor
: Jehan

Student
: Darshan Kadu (darshan)

Category
: Plug-Ins

Status
: finished

Integration status
: finished

One of the blockers of the GIMP 2.10 release was to port our JPEG2000 plug-in
from `jasper` to `openjpeg` library.
16/32 bit support for JPEG2000 was also a secondary target.

See also:

* ["JPEG 2000 support ported to OpenJPEG" in 2.10.0rc1 release
  news](https://www.gimp.org/news/2018/03/26/gimp-2-10-0-rc1-released/#jpeg-2000-support-ported-to-openjpeg)

## Projects in GSoC 2013
### Implement GEGL ops that GIMP needs (OpenCL ports)

Mentor
: Victor Oliviera (victorm)

Student
: Carlos Zubieta (zurwolf)

Category
: Plug-Ins/Scripts

Status
: finished

Integration status
: finished

The release of GIMP 2.10 is partially blocked by legacy plug-ins. Some of them
have to be ported to GEGL operations, some can just work on GEGL buffers for
now.
Since 2011 with the participation of Victor Oliveira at the GSoC, the “new” GIMP
core library, GEGL, has support for OpenCL. A lot of work has been done to give
full OpenCL support to GEGL, but there still much more to do. Some of the lacking
OpenCL support parts are the GEGL operations. I propose to port to OpenCL the
existing GEGL operations list on the wiki at the processing, color grading and
effects section of the Porting filters to OpenCL page.


Minimal Student Requirements:
* Good C knowledge
* basic GObject knowledge

See also
* [GSoC page](http://www.google-melange.com/gsoc/project/google/gsoc2013/zurwolf/8001)
* [Victor Olivieras OpenCL GSoC 2011 project](#opencl-in-gegl)
* Victor Olivieras presentation at LGM: [Slides](http://www.slideshare.net/lgworld/implementing-opencl-support-in-gegl-and-gimp#btnNext), [Video](http://www.youtube.com/watch?v=UzBPUalkqg8&feature=plcp)
* [GIMP mailing list discussions (OpenCL)](http://gimp.markmail.org/search/gegl+OpenCL)
* [GIMP mailing list discussions (GPU)](http://gimp.markmail.org/search/gegl+GPU)
* [GEGL mailing list discussions (OpenCL)](http://gegl.markmail.org/search/OpenCL)
* [GEGL mailing list discussions (GPU)](http://gegl.markmail.org/search/GPU)
* [Repository branch](https://git.gnome.org/browse/gegl/log/?h=soc-2013-opecl-ops)
* [GEGL and OpenCL porting matrix](http://wiki.gimp.org/index.php/Hacking:Porting_filters_to_GEGL)

### Implement the combined selection tool

Mentor
: Michael Natterer (mitch)

Student
: Ajay Ramanathan (Chinu)

Category
: Tools, User Interface

Status
: not finished

Integration status
: not integrated

This projects aim is to implement a unified selection tool that combines the
functionality of the rectangle/ellipse selection tool, fuzzy select, select by
color, foreground selection tool, free selection tool and intelligent scissors
into three new tools, and is easier and faster to use. The combined selection
tool should be an all-in-one tool for selecting regions based on [an interaction
design research](http://blog.mmiworks.net/2011/08/teaching-interaction-10.html)
made by Voralberg students in 2010.

Minimal Student Requirements:
* Good C knowledge
* Basic Gtk+/GObject knowledge and/or at least some interest in User Interface programming

See also
* [GSoC page](http://google-melange.appspot.com/gsoc/project/google/gsoc2013/chinu_93/4001?ModPagespeed=noscript)
* [Specification Draft](https://github.com/AjayRamanathan/soc-2013-combined-selection-tool/blob/master/Documentation/Modified%20tools.md)
* [GIMP mailing list discussions](http://gimp.markmail.org/search/combined+selection)
* [Repository branch](https://git.gnome.org/browse/gimp/log/?h=soc-2013-combined-selection-tool)
* [External project repository](https://github.com/AjayRamanathan/soc-2013-combined-selection-tool)

### Improve support for importing and exporting PSD files

Mentor
: Michael Schumacher (schumaml)

Student
: Simon Lui (Crystallis)

Status
: finished

Integration status
: finished

While existing PSD plug-in supports many essential features of the file format,
fixing bugs and adding support for some missing features would make a rather
useful GSoC project.

Note that until non-destructive editing is at least designed for GIMP, it isn't
possible to implement support for layer styles or adjustment layers. The code,
however, should be ready for that.

Additional work could involve (better) support for resources, such as:

* Photoshop gradients ([reverse-engineered](https://gitorious.org/re-lab/graphics/blobs/raw/master/photoshop/grd/grd-spec.htm))
* Photoshop patterns (merge/adapt [existing PAT loader](http://registry.gimp.org/node/10879))
* brush dynamics in ABR files ([reverse-engineered](https://gitorious.org/re-lab/graphics/blobs/raw/master/photoshop/abr/abr-spec.htm))

Minimal Student Requirements:

* Good C knowledge
* Highly recommended — some knowledge of GIMP's internals (core)

See also:

* [GSoC page](http://google-melange.appspot.com/gsoc/project/google/gsoc2013/crystallis/10001?ModPagespeed=noscript)
* [PSD specification](http://www.adobe.com/devnet-apps/photoshop/fileformatashtml/)
* [PSD related bugs in GIMP](https://bugzilla.gnome.org/buglist.cgi?quicksearch=product%3A%22GIMP%22+PSD)
* [GIMP mailing list discussions](http://gimp.markmail.org/search/PSD)
* [Repository branch](https://git.gnome.org/browse/gimp/log/?h=soc-2013-psd)
* [call for testers and
  contributors](https://mail.gnome.org/archives/gimp-developer-list/2014-February/msg00085.html)

### n-Point Image Deformation Tool

Mentor
: Alexia Death (Alexia_Death)

Student
: Marek Dvorožňák (korek)

Category
: Tools

Status
: finished

Integration status
: playground (stability and performance issues)

During image editing it's very useful to have a possibility to deform an image in a way so that it behaves like a real world object (or object made of a rubber). At present there are many image deformation methods allowing us to do that. Some of them are implemented as tools in various software - for example Puppet Warp in Photoshop. I propose to implement a method into GIMP which behaves similarly to these methods but is more intuitive (easier to use), more predictable and should be fast enough.

See also
* [GSoC page](https://google-melange.appspot.com/gsoc/proposal/review/google/gsoc2013/dvoromar/1?ModPagespeed=noscript)
* [Description of features](http://wiki.gimp.org/index.php/Mindstorm:n-point_image_deformation_tool)
* [Specification of the similar Cage Tool](http://gui.gimp.org/index.php/Cage_tool)
* [GIMP UI brainstorm idea 'Bend in curves'](http://gimp-brainstorm.blogspot.de/2010/02/bend-in-curves.html)
* [GIMP mailing list discussions](http://gimp.markmail.org/search/deformation+tool)
* [Youtube demo video](http://www.youtube.com/watch?v=k3gN6aZWc-8&feature=youtu.be)
* [Repository branch (GEGL part)](https://git.gnome.org/browse/gegl/log/?h=soc-2013-n-point-deformation)
* [Repository branch (GIMP part)](https://git.gnome.org/browse/gimp/log/?h=soc-2013-n-point-deformation-tool)

## Projects in GSoC 2012
### Port missing GIMP features to GEGL

Mentor
: Mukund Sivaraman

Student
: Ville Sokk

Integration status
: finished

Working on recreating legacy GIMP features that are missing from GEGL. The aim
is to help replace the existing GIMP image processing core with GEGL to bring
better precision, more colour formats and better programmer flexibility to GIMP
while keeping all the familiar GIMP features working as they are.

See also:

* the [GSoC page](http://www.google-melange.com/gsoc/project/google/gsoc2012/embassyhill/9001)
* [Google Summer of Code 2012
  Results](https://www.gimp.org/news/2012/09/03/google-summer-of-code-2012-is-over/)

### Porting GEGL operations

Mentor
: Victor Oliviera

Student
: Hans Lo

Integration status
: finished

I am applying for the GEGL operation porting project.

See also:

* [GSoC page](http://www.google-melange.com/gsoc/project/google/gsoc2012/hansshulo/17002)
* [GEGL-repository branch](https://git.gnome.org/browse/gegl/log/?h=soc-2012-ops)
* [Google Summer of Code 2012
  Results](https://www.gimp.org/news/2012/09/03/google-summer-of-code-2012-is-over/)

### Graphical Node Editor

Mentor
: Alexia Death

Student
: Isaac Wagner

It consists of two components. The first and largest component is the
development of a fully-featured graphical node editor ("box and hose editor")
which will sit on top of the GEGL public API and allow for fast sandboxing of
GEGL operations for debugging, artistic, and showcase purposes. The editor will
be similar to familiar editors such as Blenders shader editor and the MathMap1
editor. The second component of my proposal is to develop code for saving and
loading meta-ops to and from XML (resolving bug 465743). This code will exist
within the GEGL codebase and manifest itself as a component of the node editors
interface.

See also:

* [GSoC page](http://www.google-melange.com/gsoc/project/google/gsoc2012/isaacbw/10001)
* [GEGL-repository branch](https://git.gnome.org/browse/gegl/log/?h=soc-2012-editor)
* [Project repository](https://git.gnome.org/browse/gegl-edit/)
* [Google Summer of Code 2012
  Results](https://www.gimp.org/news/2012/09/03/google-summer-of-code-2012-is-over/)

To build the project yourself, you also need [Graph-gtk](https://git.gnome.org/browse/graph-gtk).

### Unified Transformation Tool

Mentor
: Michael Natterer

Student
: Mikael Magnusson

Category
: Tools, User Interface

Integration status
: finished (integrated in GIMP 2.10)

Implement a unified transformation tool that combines the functionality of the rotate, shear, scale and perspective tools, and is easier and faster to use.

Initial implementation is available in [Mikael Magnusson's Git branch](http://cgit.mika.l3ib.org/cgit/gimp/log/?h=mika-topics/hack-perspective).

Minimal Student Requirements:
* Good C knowledge
* Gtk+/GObject basic knowledge and/or at least some interest in User Interface programming

See also:

* [GSoC page](http://www.google-melange.com/gsoc/project/google/gsoc2012/mikachu/10001).
* [Specification](http://gui.gimp.org/index.php/Transformation_tool_specification)
* [Repository branch](https://git.gnome.org/browse/gimp/log/?h=soc-2012-unified-transformation)
* [Google Summer of Code 2012
  Results](https://www.gimp.org/news/2012/09/03/google-summer-of-code-2012-is-over/)

## Projects in GSoC 2011
### Porting GIMP plugins to GEGL operations

Mentor
: Mukund Sivaraman

Student
: Robert Sasu

Integration status
: finished (integrated in GIMP 2.10)

The main point is to port GIMP plug-ins to GEGL, because GEGL API supports higher pixel depth, has a smart job and memory management, allows non-destructive editing.

See also
* [GSoC page](http://www.google-melange.com/gsoc/project/google/gsoc2011/robert_sasu/17001)
* [Repository branch](http://www.google-melange.com/gsoc/project/google/gsoc2011/robert_sasu/17001)

### OpenCL in GEGL

Mentor
: João S. O. Bueno

Student
: Victor Oliveira

Integration status
: finished (integrated in GIMP 2.10)

The proposal is about making it possible to write GEGL operations in OpenCL. There are two tasks:

* Automatic memory management and migration of tiles from GPU to the CPU and the other way around.
* Make some GEGL operations in OpenCL.

See also
* [GSoC page](http://www.google-melange.com/gsoc/project/google/gsoc2011/victor_matheus/8001)
* [Repository branch](https://git.gnome.org/browse/gegl/log/?h=opencl-ops)

### Adaptive Cloning (aka Seamless cloning)

Mentor
: Michael Natterer

Student
: Barak Itkin

Integration status
: playground (stability and performance issues)

Direct cloning of parts from one image to another, usually ends in bad results
because of different lighting conditions and other settings (such as
white-balance) which causes the color of the cloned part not to match the source
image and look out of place. There are some techniques to solve this, by using
poisson equations and some other methods. This suggestion allows a fast
(realtime) solution for allowing to paste seamlessly parts from one image to
another.

See also
* [GSoC page](http://www.google-melange.com/gsoc/project/google/gsoc2011/lightningismyname/6001)
* [GEGL-repository branch](https://git.gnome.org/browse/gegl/log/?h=soc-2011-seamless-clone)
* [GIMP-repository branch (1)](https://git.gnome.org/browse/gimp/log/?h=soc-2011-seamless-clone)
* [GIMP-repository branch (2)](https://git.gnome.org/browse/gimp/log/?h=soc-2011-seamless-clone2)
* [A Youtube demo video](http://www.youtube.com/watch?v=pqVMt-ReaDc)

Barak Itkin recently posted an update in [his blog](http://lightningismyname.blogspot.ru/2013/05/gimp-gsoc-2011-seamless-cloning-project.html)  to outline the changes to be made.

### Replace GimpSizeEntry widget

Mentor
: Martin Nordholts

Student
: Enrico Schröder

Integration status
: work in progress

The GimpSizeEntry widget in its current form, which is used across the user
interface for entering values and their corresponding units (e.g. image
dimensions, lengths, etc.), has a couple of issues both from a users and a
developers standpoint. The project is to rewrite the widget being based on
GtkEntry with the ability to directly enter and display the desired unit in the
text field itself instead of in an external control. Simple mathematical terms
for the input of values in different units will be possible using the existing
parser (gimpeevl).

See also
* [GSoC page](http://www.google-melange.com/gsoc/project/google/gsoc2011/enrico_schroeder/12001)
* [Repository branch](https://git.gnome.org/browse/gimp/log/?h=soc-2011-gimpunitentry)

### Warp tool for GIMP

Mentor
: Alexia Death

Student
: Michael Muré

Integration status
: finished (integrated in GIMP 2.10)

The goal is to implement a live on-canvas iWarp like tool. Behavior: The basic usecase of this tool would be:

1. The user select the iwarp tool. Nothing is displayed on top of the image.
2. If needed, the user select a particular behavior for the tool (drag,shrink, grow, whirl, pinch ..)
3. The user do a stroke on the canvas. The image is updated accordingly.

See also
* [GSoC page](http://www.google-melange.com/gsoc/project/google/gsoc2011/michael_mure/5001)
* [Specification](http://gui.gimp.org/index.php/Warp_tool_specification)
* [GEGL-repository branch](https://git.gnome.org/browse/gegl/log/?h=soc-2011-warp)
* [GIMP-repository branch](https://git.gnome.org/browse/gimp/log/?h=soc-2011-warp)

## Projects in GSoC 2010

### HDR and matting operators for GEGL

Mentor
: Martin Nordholts

Student
: Danny Robson

Integration status
: finished (integrated in GIMP 2.10)

GEGL does not currently contain methods which assist in HDR workflows which have
become quite popular. I propose to develop three native GEGL tone-mapping
operations.

I also propose to target an image matting operator, allowing the straightforward
decomposition of an image into foreground and background components. I will
implement the `matting laplacian'.

* [Google Summer of Code 2010
  Results](https://www.gimp.org/news/2010/10/04/google-summer-of-code-2010-results/)

### Cage-based deformation tool

Mentor
: Kaja Liiv

Student
: Michael Muré

Green Coordinates is a method allowing deformation of an image, by deforming the
inside of a polyhedral cage. The technique was exposed in Siggraph 2008. Unlike
the other classical method (mean value coordinates, harmonic coordinates, ..),
it allows high quality deformation by preserving the shape.

Basic behavior of the tool:

* you "put" a closed polygon on the image (not limited to 4 handles)
* you deform the cage, the image is deformed accordingly

See also:

* [Google Summer of Code 2010
  Results](https://www.gimp.org/news/2010/10/04/google-summer-of-code-2010-results/)

### Implement and add infrastructure for on-canvas tool drawing/options

Mentor
: Joao Bueno

Student
: Chen Yan

Status
: unsuccessful

The project is to implement and add infrastructure for on-canvas tool
drawing/options. Gimp has many dockable dialogs. If some commonly used and
simple interactions, with values and settings, can be presented, tracked, and
modified directly on the image canvas, it would be very useful. It would save
clicks and users' time. Reducing the number of opened dockable dialogs is
advantageous, especially for Windows users, for many simple editing, drawing and
options.

* [Google Summer of Code 2010
  Results](https://www.gimp.org/news/2010/10/04/google-summer-of-code-2010-results/)

## Projects finished in GSoC 2009
### Better nonlinear resampler with built-in antialiasing (GEGL)

Mentor
: Nicolas Robidoux

Student
: Adam Turcotte

Port improvements to Nohalo Level 1 and port Snohalo Level 1 from VIPS to GEGL. Program Nohalo and Snohalo Levels 2 and 3 in demand-driven mode for GEGL with Dr. Robidoux. Test for bugs and optimize for speed. Merge the various methods so that levels are automatically chosen depending on requested quality level, and so Nohalo is selected instead of Snohalo when additional antialiasing is set to zero. Sample output images can be seen at http://gsoc.rivetsforbreakfast.com/enlargements.html.

### OpenGL GPU-based Buffer & Operations in GEGL

Mentor
: Martin Nordholts

Student
: Jerson Michael Perpetua

GEGL (Generic Graphics Library) is a new back-end to replace GIMP's old code for handling various image processing tasks.

GPUs nowadays are capable of massively accelerating graphics tasks, there is an ongoing movement to harness this power in general computing. However, because of the nature of GPU parallelization, not all code can be executed in the GPU. GEGL's architecture of pixel operations yields itself to GPU parallelization, making it a good candidate for GPU acceleration.

### Fast Adaptive Resampler Tailored For Transformations Which Mostly Downsample (GEGL)

Mentor
: Nicolas Robidoux

Student
: Eric Daoust

I am proposing to implement better and faster downsampling and resampling in GEGL

### Improve Foreground Selection Tool

Mentor
: Gerald Friedland

Student
: Jie Ding

SIOX is the algorithm underlying GIMP's foreground selection. In current version, a pixel is either foreground or background. A Detail Refinement Brush will be offered to the user as a simple interactive drawing tool, with which the foreground selection could implement a soft segmentation. It would improve the performance of the tool for areas where background and foregound overlap, such as in fine-grained, highly-textured, or semi-transparent objects like hair, clouds, or iced objects.

### Proposal - Advanced GUI for brush dynamics (GIMP)

Mentor
: Kaja Liiv

Student
: Zhenfeng Zhao

This proposal presents the plan and goals to the project “Advanced GUI for brush dynamics”, and how my experience fits. With my graduate studies in image processing, and being Lead Developer of a local company (3-patent image manipulation R&amp;D) I had outstanding skills and experiences. I am proficient in C, understand the tasks, and enjoy working virtually.

## Projects finished in GSoC 2008
### Improving the text-tool in GIMP

Mentor
: Daniel Eddeland

Student
: William Skaggs

"Although the text-tool in GIMP works right now, it could be even better and more convenient with more functionality. When editing text you type the text in a dialog instead of on the canvas itself. I found "on-canvas text editing" as one of the ideas listed on the GIMP GSoC ideas page, and thought it would be a good idea to implement, as it would make the text tool more ergonomic and also look a little better (less dialogs to keep track off = cleaner interface)."

### Enhance Python Scripting Interface Experience

Mentor
: João Sebastião de Oliveira Bueno

Student
: Lars-Peter Clausen

"The overall goal of this project is to enhance the python scripting interface experience for The GIMP.

As part of this I'll creating binding for gimp objects and widgets that haven't a binding yet but would be useful to have in python scripts. I'll also in collaboration with the community identify other missing or nice to have aspects in the python scripting interface and then try add them."

### Tagging of GIMP Resources

Mentor
: Aurimas Juska

Student
: Sven Neumann

"Currently resources such as brushes, gradients, etc are presented to the user in a single list without a possibility to organize them in any way. This makes it impossible to deal with large number of resources efficiently, thus making the GIMP less attractive for both professional and casual users.

Adding tags to resources is natural and convenient way for users to organize their collection of resources so they could quickly find the ones they want for the specific task and create their own collections easily."

### Operations in Frequency Domain

Mentor
: Øyvind Kolås

Student
: Zhang Junbo

"Many of the image processing operations in the spatial domain are very difficult or even impossible, but in the frequency domain it may be quite simple. But GEGL, the new framework of GIMP, has not support operations in frequency domain yet. Therefore, a collection of operations in frequency domain for GEGL is urgently needed.

A good framework has been provided in GEGL. Operations(plug-ins) in frequency domain could be implemented modularly."

## Projects finished in GSoC 2006
### JPEG2000 plug-in

There is a number of file formats that GIMP should support, but doesn't or at least doesn't support fully, for example JPEG 2000.

### Vector layers and tools to manipulate them

Mentor
: Simon Budig

Student
: Hendrik Boom

Have Vector layers that are connected to (multiple) vectors objects (think paths) and get re-rendered when the vectors change. Make it possible to attach styles to the vectors and maybe add different types of vector shapes (rectangles, ellipses...). Make all this usable...

* [Developer log](https://web.archive.org/web/20061219233008/http://lunarcrisis.pooq.com/wiki/Gimp/SoC2006Log)

### Ruby binding for GIMP scripting

Mentor
: Kevin Cozens

Student
: Scott Lembcke

Ruby is an object-oriented language that is driven by the „principle of least surprise“ - it tries to avoid all the pitfalls and caveats other languages have. There is an ancient version of a GIMP binding, it would be interesting to have it resurrected (with as little undead side effects as possible

### Healing brush

Mentor
: Manish Singh

Student
: Kevin Sookocheff

Removing freckles or other spots from skin can be tedious - the clone tool helps, but sometimes it is hard to find the right spot to clone from. A "healing brush", as it is named in other programs, removes the spots by e.g. averaging the surrounding area; other approaches are possible as well.

### Vanishing point cloning

Mentor
: Manish Singh

Student
: Pedro Alonso

Photoshop got an interesting tool recently - similar to clone, but it applies a perspective transform to the texture. Think about changing or "repairing" a tiled floor of a hallway on a photo - it is easy to create a flat tile texture, but it has to be transformed correctly when applying it
