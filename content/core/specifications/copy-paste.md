---
title: "Copy/Cut - Paste image data"
Author: "Aryeom and Jehan"
Date: 2022-11-07
---

This document details the logic for copy|cut-pasting image data. Copy-pasting is
a very common usage accross programs so specifications may seem uselessly
complicated.
Yet there are a lot of additional concepts which need to be taken into accounts.

## Copy
### Copying full drawables

A "drawable" in GIMP can be a layer, a named channel or a layer mask. E.g. you
can copy then paste a full layer within the layer stack, which is equivalent to
duplicating the layer. This is the simplest case. There are many more specific
use case which we will list later in this specification.

Copying full drawables will happen when there is no selection in the image and
when you run the "Copy" action (by default `Ctrl-c` or `Edit > Copy`). It will
then copy all the selected drawables, which can be either 1 or more layers, 1 or
more channels or a single layer mask.

### Copying drawable selection

If you have a selection, then it will copy a piece of each selected layer, and
will also keep them as individual layers, the size of the selection's bounding
box.

### "Copy Visible" variant

The "Copy Visible" also works with or without selection. The main difference
with the normal "Copy" action is that it creates only a single image in memory.
The selected drawables at time of copy don't matter, only the merged down
projection of the image within the selection (or the full image if there is no
selection).

## Cut

The "Cut" action is the same as "Copy" except that it will also delete the
contents of the selected layers, within the pixel selection.

## Paste

Once image data was copied and is made available through the clipboard (either
by GIMP itself or by other software), GIMP is able to make use of it.

Image data which was copied by GIMP itself will contain a bit more metadata
usable by the program when we paste it back. The main difference with
out-of-process data is that it may contain several images (layers) and that each
image will have associated offset.

Also it is ensured that color conversion will be correct when copying from GIMP
to GIMP, whereas a copy from a third-party program might have issues for
non-sRGB conversion.

Out-of-GIMP data will be an image buffer. In particular, it is not "multi-layer"
and has no associated offset. Therefore the current positionning algorithm are
only based on the image you are working on.

### Pasting when layers are selected

If one or several layers are selected when you paste, all the copied layers will
be pasted over the top selected layer (visually in the Layers dockable), in the
same order as in Copy time.

The "Paste as Single Layer" variants will merge all the copied layers into a
single layer instead of several pasted layers (default "Paste" actions).

### Pasting when a layer mask is selected

Since you can only select one layer mask at once, pasting will create a
"Floating mask" above the layer mask. It allows to move or edit the floating
mask with immediate preview of the change.

The floating mask will contain a merged-down grayscale content of the copied
contents. This also means that the "Paste as Single Layer" variants will work
the same as the default "Paste" actions.

While a floating mask exist, no drawable selection change can be done until one
of the following actions is done:

* "Anchor the floating mask" the floating mask and the current layer mask are
  merged.
* Delete: the floating mask is deleted.
* Undo: similar result to deleting the floating mask, except that we also remove
  the undo step.

### Pasting when channels are selected

It is currently only possible to paste pixel data as layer(s) or to be merged
with an existing layer mask (color spaces are converted accordingly). In
particular, it is currently not possible to paste directly as a channel (even
though you may do it in several steps, e.g. through transforming grayscaled
pixel data to selection).

Therefore even if you are currently selecting channels when you run a "Paste"
action, then the paste will happen at the top (visually in the Layers dockable)
of the layer stack, at root level.

### Position of the pasted data

#### "Paste" (`Ctrl-V`) and "Paste as Single Layer"

* When pasting over one or several drawables (layers or layer mask):
    * The source dimension is the bounding box of the pasted data. In case
      several pieces of layers were pasted, the relative positions are kept, so
      that the source dimensions may end up bigger than any of the individual
      layers. Moreover the offsets are the smaller in both dimensions.
    * The target is the intersection of all selected drawables' bounds with the
      image selection bounds (the whole image in case of no image selection).
      *As a special case, if only a single group layer containing no children
      layers is selected, then the selection bounds itself is the target.*
    * If the image has no selection, we are zoomed-in (the viewport shows less
      than the full image) and the target intersects with the viewport, then we
      center the paste on the viewport.
    * Otherwise we center the paste on the target.
* If no drawables are selected (which should likely never happen):
    * If the image is not fully visible in the viewport and the paste is smaller
      than the image:
        1. the buffer is pasted in the center of the viewport.
        2. if the buffer dimensions are too large so that the paste is not fully
           visible on canvas, it is offsetted to fit-in.
        3. If the buffer is simply to large for the image, it is pasted at
           dimension `(0, 0)`.
      Note that these offsetting rules are per-dimension. It is possible that
      only one of the 2 dimensions get offsetted this way.
    * Otherwise, center on the image.

Note: if the paste is always the size of the selection, a direct copy-paste
without editing the selection is equivalent to a Paste in Place.
Also it stays consistent for the case where a selection and source drawable
don't intersect.

#### "Paste in Place" (`Ctrl-Alt-V`) and "Paste as Single Layer in Place"

When you paste "in place" layers or selected pieces of layers, copied from GIMP,
then the exact same offset as the one from "Copy" time is used. Neither the
position, the zoom on the canvas nor the selection matter.

On the other hand, if you paste "in place" image data from outside GIMP, since
there is no "place" in a raw image buffer, it means pasting at `(0, 0)`, i.e.
that the top-left corner of the pasted buffer will coincide with top-left corner
of your image canvas.
