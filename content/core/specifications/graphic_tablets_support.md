---
title: "Graphic tablets, pen displays and touch screen support"
Author: "GIMP team"
Date: 2022-09-11
---

ℹ️ **Note**: the current document so far is mostly "current state" information on
tablet support in GIMP (stable and development). Ideally we should move the
document into another direction, to be about "how we want tablet support to be".
Basically it should rather be a specification on stylus and gesture support (and
related technologies) in GIMP.

## Stylus usage
### GIMP 2.10

GIMP 2.10 doesn't have hotplug support for GIMP. It means you have to
plug your tablet before starting GIMP and avoid unplugging it while GIMP
is still running.

Also you need to specifically activate your stylus by setting its "Mode"
to "screen" in `Edit > Input Devices` dialog.

See for instance [Huion
instructions](https://support.huion.com/en/support/solutions/articles/44002277605-how-to-setup-gimp-pen-pressure-on-macos)
(these instructions would be similar on all OSes).

#### Linux

GIMP maintainers, Aryeom and Jehan, have worked with graphic tablets on
GIMP since 2013. They used in turn an old Wacom Bamboo MTE 450, Intuos
5, Intuos Pro M, Cintiq, Mobile Studio… On X11, there are no major
problems to report.

On Wayland, various issues, sometimes blocking ones, have been reported,
and since then fixed for the most. We don't use Wayland for daily work
so can't be sure, but support seems like it would be reasonable
nowadays.

#### Windows

As a general rule, GIMP 2.\* on Windows supports every tablet with a
`Wintab` driver. Nowadays some of the newest tablets don't come with the
Wintab driver activated by default, but they often have one if you
search on the constructor website, or as a switch in settings.

The below table is legacy and kept here in case anyone wants to update
it. Nevertheless it is very incomplete and possibly wrong.

| Brand | Model | Version of GIMP | Version of OS | Status |
| ----- | ----- | --------------- | ------------- | ------ |
| Genius | MousePen 8x6   | 2.10.6  | ?           | Not supported |
| Huion  | 1060P          | 2.10.8  | 7           | Not supported |
| Wacom  | One (CTL-671)  | 2.10.8  | 7           | Supported     |
| Wacom  | Mobile Studio  | 2.10.\* | 7           | Supported     |
| Wacom  | Intuos Pro     | 2.10.\* | 7           | Supported     |
| Wacom  | Cintiq Pro     | 2.10.\* | 7           | Supported     |
| Wacom  | Bamboo MTE 450 | 2.10.\* | 7           | Supported     |

#### macOS

Though we do have bug reports at time, involving tablets, they are
rarely about the tablet not working at all (see [bug reports involving
macOS and graphic
tablets](https://gitlab.gnome.org/GNOME/gimp/-/issues/?sort=updated_desc&state=all&label_name%5B%5D=OS%3A%20macOS%20%2F%20OSX&label_name%5B%5D=Environment%3ATablets&first_page_size=20)).

GIMP contributors (Aryeom as teacher, and Jehan as assistant) have also
been giving university classes with GIMP, for digital illustrations and
retouching. Students all have graphic tablets. No specific issues were
reported.

As a consequence, we assume GIMP 2.10 has an acceptable graphic tablet
support on macOS.

### GIMP 2.99/3.0

GIMP 2.99/3.0 come with proper hotplug. You don't need anymore to play
with the input device mode anymore. Normally all it takes for you to be
able to paint with pressure, tilt or other pen features, is to plug your
tablet and start painting.

Of course, it also means you need to use an adequate dynamics, making
usage of pressure/tilt, for most paint tools (though some paint tools
work differently: the Ink tool has core pressure support; MyPaint tool
stylus support only depends on the used brush).

#### Linux

GIMP maintainers, Aryeom and Jehan, have worked with graphic tablets on
GIMP since 2013. They used in turn an old Wacom Bamboo MTE 450, Intuos
5, Intuos Pro M, Cintiq, Mobile Studio… They moved to using development
2.99 versions, a few years ago. On X11, there are no major problems to
report.

On Wayland, various issues, sometimes blocking ones, have been reported,
and since then fixed for the most. We don't use Wayland for daily work
so can't be sure, but support seems like it would be reasonable
nowadays.

#### Windows

Whereas GTK had historically only support for Wintab drivers, this
changed for [GTK
3.24.30](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/1563) which
added support for Windows Pointer Input Stack, based on [Windows Ink
backend](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/1563#note_767239),
which is why the names are often used interchangeably.

Starting GIMP 2.99.8, GIMP detects API support and use Windows Ink
(prefered) if available and Wintab as fallback.

Since we know that the Windows Ink API has sometimes issues, we also
added a settings in `Preferences > Input Devices`, allowing to manually
choose the API you want, when several are available.

As far as we understood, Windows Ink API should [work on Windows
8+](https://gitlab.gnome.org/GNOME/gtk/-/issues/262#note_713393) and
should not require specific drivers [for recent enough
tablets](https://gitlab.gnome.org/GNOME/gimp/-/issues/7197#note_1259860),
though it may require a "HID filter driver" for older hardware.

Future possible work:

* adding [RealTimeStylus
API](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/458#note_1248705) support.
It can be interesting for cases such as using GIMP on [Windows 7 without
a Wintab driver installed](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/458#note_1255848).
In this case, only the RealTimeStylus API is an option.

#### macOS

Though we do have bug reports at time, involving tablets, they are
rarely about the tablet not working at all (see [bug reports involving
macOS and graphic
tablets](https://gitlab.gnome.org/GNOME/gimp/-/issues/?sort=updated_desc&state=all&label_name%5B%5D=OS%3A%20macOS%20%2F%20OSX&label_name%5B%5D=Environment%3ATablets&first_page_size=20)).

GIMP contributors (Aryeom as teacher, and Jehan as assistant) have also
been giving university classes with GIMP, for digital illustrations and
retouching. Students all have graphic tablets. In the 2021-2022 school
year, some students even worked with 2.99 development versions. No
specific issues were reported.

Our macOS build maintainer, Lukas Oberhuber, also reported proper
support when testing a [Wacom Intuos on M1 Mac in emulated
mode](https://gitlab.gnome.org/Infrastructure/gimp-macos-build/-/issues/40#note_1538829):

> Can confirm that I am able to get pressure sensitivity with a Wacom
> Intuos on M1 Mac in emulated mode.
>
> Performance is good and seems to work (I've not used tablets much so I
> might be missing something).

As a consequence, we assume GIMP 2.99 has an acceptable graphic tablet
support on macOS.

Future possible work:

* [Sidecar support](https://gitlab.gnome.org/GNOME/gimp/-/issues/4971)

## Gestures in GIMP 2.99

The feature was not possible using GTK+2 so support only started with
GIMP 2.99 (which will become GIMP 3.0 when stable).

Povilas Kanapickas added [zooming
gestures](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/401) for
the canvas, with 2 fingers, in GIMP 2.99.6, then [rotation
gesture](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/597) with
2 fingers in GIMP 2.99.12.
These 2 gestures are exclusive, depending on how the gesture was
initially recognized. This is a choice on purpose because we thought
that people may not want both rotation and zoom in the same time, but it
can always be re-evaluated. Also it currently only works using touchpad.
Not directly in canvas with a touchscreen.

There is also gesture support for [zooming in
`GimpContainerTreeView`](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/599)
(e.g. Layers, Channels, Vectors dockable), to increase the preview size
in the tree listing. Finally there is gesture support for [zooming in the
gradient editor](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/615).

Note that we don't have proper on-canvas gesture for touchscreens yet.
I.e. that currently, fingers on touchscreens are their own input device
and will simply apply the tool attached to the device (drawing, etc.).
It has often been discussed, e.g. in this [MR
thread](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/486#note_1286056)
and with Aryeom. Basically good support could be:

* There should be a way to disable touch altogether in the software.
  Some hardware have physical switch for this, but not all of them. OSes
  or desktops also usually have settings, but you might want to disable
  it only for GIMP. Wandering fingers adding random pixels, or even
  unexpected zooming or panning is annoying for some people who do
  everything with their stylus.
* What we really want is a switch to either use touch for tool actions,
  or to switch it for canvas navigation: panning, rotating and zooming
  in particular. What should be the specific gesture is to be discussed,
  as I thought that the number of fingers could determine this (1
  finger: panning; 2 fingers: zooming; 3 fingers: rotating). But
  apparently some OSes want to reserve 3-finger touch for OS actions.
  Currently, touchpad zoom and rotation are both handled with 2-finger
  touch.
* There was also the talk of having both painting/tool action and canvas
  navigation active. In particular: 1-finger touch would be tool action,
  whereas 2-finger touch would be canvas navigation. To be tested but I
  believe that if we had this, it should be optional too. Some people
  may want tool action or canvas navigation only.
* Other actions may be interesting too: swipe (3-finger?) to move from
  one opened image to another? Or to navigate through layers? And other
  canvas navigation actions like quick canvas mirroring may be worth it
  too.

This is all in discussion and we welcome patches for this to happen.
