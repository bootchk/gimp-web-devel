+++
title = "API References"
date = "2022-08-07"
abbrev = "api-ref"
description = "GIMP Reference Manuals"
weight = 1
toc = false
+++

GIMP comes with libraries which can be used by plug-in developers to
enhance their usage of the software. This page lists documentations for
the `Application Programming Interfaces (API)` of these libraries:

* [GIMP 2.0 API](/api/2.0/): for versions 2.0 up to 2.10.
* [GIMP 2.99 API](/api/3.0/): experimental API (moving target) for future
  version 3.0.

Additionnally you may want to use some of the following interfaces:

* [Babl API](/api/babl/): GIMP's pixel encoding and color space conversion
  engine.
* [GEGL API](/api/gegl/): GIMP's image processing engine.
* [GTK, Pango, GdkPixbuf and more…](https://docs.gtk.org/): more dependencies
  used by GIMP and `libgimp`.

## Offline API references
### Download
If you don't want to read the API documentation online, you may also
download the source tarballs from our [download
server](https://download.gimp.org/pub/gimp/v2.10/). They contains
pre-generated API documentation in HTML format in
`devel-docs/libgimp*/html/` folders.

On distribution, the API reference HTML may also be provided with the
development package or as separate packages. For instance, on Debian,
the package `libgimp2.0-doc` would usually be the one containing these
files.

For GIMP 3.0, the pre-generated documentation will be in a dedicated
tarball.

### Generate

If you wish to generate them, as part of your development process, here
is the procedure, as part of the rest of the [building
procedure](/core/setup/build/):

#### GIMP 2.10

1. Configure with `--enable-gtk-doc` and `--enable-gtk-doc-app`:

```sh
configure --prefix=$GIMP_PREFIX --enable-gtk-doc  --enable-gtk-doc-app
```
If you got the sources directly from the git repository rather than a
tarball, use `autogen.sh` instead of configure.

The parameters have the following meaning:

`--enable-gtk-doc`
: use gtk-doc to build documentation

`--enable-gtk-doc-app`
: build developer documentation for app (not only the `libgimp*` libraries).

To find out more parameters, run `configure --help`
`GIMP_PREFIX` is a variable for the directory you will install GIMP in,
as explained in the [Building GIMP](/core/setup/build/) page.

2. Build GIMP normall:

```sh
make
make install
```

3. Find the generated documentation:

```sh
cd $GIMP_PREFIX/share/gtk-doc/html
```

4. From the desired subdirectory open the file `index.html` for an overview of that module.

#### GIMP 2.99
##### Meson build system

Similarly the `meson` build provides the option `-Dgi-docgen=enabled` to
make sure the documentation built by `gi-docgen` are enabled. These are
the main documentation for the `libgimp` and `libgimpui` APIs.

We also have a `-Dg-ir-doc=true` option which will build language
specific API with `yelp` and `g-ir-doc-tool`, for some of the languages
supported by GObject Introspection. We are still pondering whether these
documentation really bring much, over using the main API references and
some per-language tutorial explaining how to port each calls to specific
bindings.

##### Manual `g-ir-doc` generation

This is what `-Dg-ir-doc=true` does internally. You might want to
reproduce these steps for another language binding, to generate and
browse `libgimp` API docs, for a language binding, from your local GI
repository:

This assumes you are a builder, have installed tools for GObject
Introspection such as `g-ir-doc-tool`, have installed the GNOME help
tool yelp, and that when you built or installed GIMP, it created the
`.gir` files.

1. Run:

```sh
g-ir-doc-tool --language=Python -o ~/gimp-python-docs /usr/local/share/gir-1.0/Gimp-3.0.gir
```

The parameters have the following meaning:

`--language=Python`
: generate docs for Python programmers (try C, Lua, JS, ... ?)

`-o <path>`
: where to store the output document (in .xml format)

`/usr/...`
: the .gir xml file to generate docs from (try Gegl-1.0, Babl-1.0, ...)

2. Run:

```sh
yelp ~/gimp-python-docs
```
