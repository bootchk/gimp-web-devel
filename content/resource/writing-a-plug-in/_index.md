+++
title = "How to write a plug-in for GIMP 2.10"
description = "Writing GIMP 2.10 plug-ins"
date = "2022-07-20"
weight = 2
show_subs = false
+++

ℹ️  *The various documents listed below may not be very up-to-date. In particular,
you will not find documentation for the upcoming v3.0 API and plug-in creation
tutorials yet. We are currently in the process of rebuilding proper plug-in
developer documentation. With a bit of patience, this will soon contain
up-to-date information.* ⏳

Make good use of the [GIMP 2.0 API reference](/api/2.0),
and you can also have a look at a talk about
[GIMP plug-in programming](http://www.home.unix-ag.org/simon/gimp/guadec2002/gimp-plugin/html)
that Simon gave at GUADEC in Sevilla.


* [How to write a GIMP plug-in, part I](1/)
* [How to write a GIMP plug-in, part II](2/)
* [How to write a GIMP plug-in, part III](3/)

## Historical documentation

For historical purpose, you may also be interested into the below older
tutorials. Note that their contents may be quite outdated, yet are kept as
archive of the evolution of GIMP plug-in infrastucture.

* [Writing a GIMP Plug-In](http://gimp-plug-ins.sourceforge.net/doc/Writing/html/plug-in.html) by Kevin Turner for GIMP 1.x series (2000).
* [GIMP Python module Documentation](https://www.gimp.org/docs/python/index.html) by James Henstridge (1999)
* [GIMP Script-Fu changes in GIMP 2.4 (migration guide)](https://www.gimp.org/docs/script-fu-update.html)
