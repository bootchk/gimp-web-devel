+++
title = "How to write a GEGL filter"
description = "Writing GEGL plug-ins"
date = "2022-07-20"
weight = 3
+++

GEGL filters are also called "GEGL operations" or "GEGL plug-ins". You may see
any of these naming, though they usually refer to the same thing.

Such filters will appear automatically in the list of filters available from the
"GEGL Operation" dialog. What makes these very powerful is that they provide
automatic on-canvas previews, with split view, and they are not specific to a
version of GIMP (your GEGL operation will be usable in GIMP 2.10, 2.99, or even
any other software using GEGL if they have a generic operation interface).

For a proper introduction and an up-to-date creation process, we advise reading
this 2022/2023 tutorial by Liam Quin in 3 parts:

* [Using GEGL Plug-Ins](https://barefootliam.blogspot.com/2022/10/gegl-plug-ins-for-gimp-part-one-using.html)
* [GEGL Graph](https://barefootliam.blogspot.com/2022/12/gegl-plug-ins-for-gimp-part-two-gegl.html)
* [Writing C Plug-Ins](http://barefootliam.blogspot.com/2023/01/gegl-plug-ins-for-gimp-part-three.html)

## Installing GEGL filters

Apart from the standard operations bundled with GEGL, it will search for custom
operations in:

* Folders listed in the `GEGL_PATH` environment variable if it exists,
  overriding all other standard paths.
* The user data directory:

  * **Linux** and most **UNIX**es (except macOS, see below): `$XDG_DATA_HOME/gegl-0.4/plug-ins/`

    *Note*: this path defaults to `$HOME/.local/share/gegl-0.4/plug-ins` if
    `XDG_DATA_HOME` environment variable is not set (see the [XDG Base Directory
    Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html)).

  * **Windows**: `%LOCALAPPDATA%\gegl-0.4\plug-ins`

    *Note 1*: `%LOCALAPPDATA%` can be directly used in your file browser and
    represents the [local application
    data](https://learn.microsoft.com/en-us/windows/win32/shell/knownfolderid#FOLDERID_LOCALAPPDATA).
    <br/>Very often, the directory will be:
    `C:\Users\<UserLogin>\AppData\Local\gegl-0.4\plug-ins\`

    *Note 2*: if the `XDG_DATA_HOME` environment variable is set, it is used
    instead of `%LOCALAPPDATA%`.

  * **macOS**: `~/Library/Application Support/gegl-0.4/plug-ins/`
