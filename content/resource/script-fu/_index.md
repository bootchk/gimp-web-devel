+++
title = "Script-Fu User Guide"
description = "Writing GIMP 3 plug-ins with Script-Fu"
date = "2023-05-09"
weight = 2
show_subs = false
+++

* [Script-Fu Programmer's Reference](programmers-reference/)
* [Debugging Script-Fu Plugins](debug-script-fu/)
* [ScriptFu Tools](scriptfu-tools/)

Language Features

* [Language Overview](language-overview/)
* [Messaging functions](scriptfu-messaging/)
* [Color](color-in-script-fu/)
* [Type byte](scriptfu-byte-support/)
* [String-like objects](string-like-objects/)

Changes in ScriptFu 3

* [Overview](overview-v3/)
* [Changes in ScriptFu version 3](script-fu-changes-v3/)
* [Adapting scripts to PDB version 3](porting_scriptfu_scripts/)
* [Using the version 3 dialect for values to and from the PDB](using-v3-binding)


